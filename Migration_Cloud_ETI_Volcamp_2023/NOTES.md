---
marp: false
---

# Abstract
2 ans de migrations en environnement "cloud native" quand on n'est ni une startup, ni dans le CAC40

Depuis un peu plus de 2 ans, nous intervenons principalement sur le déploiement d'applications dites cloud native d'une ETI, sur 3 domaines :

- Gestion de la forge logicielle, essentiellement composée de logiciels libres
- Déploiement de clusters Kubernetes sur du cloud public français, plus les composants "infrastructure" des clusters.
- Mise en place d'environnement de développement, CI/CD et accompagnement des équipes de développement.

Depuis 2 ans donc, pas mal de questions, réponses, évolutions, adaptations pour faire en sorte que tout cela fonctionne au mieux : comment s'assurer que la forge tourne correctement, évolue ; comment bien automatiser les déploiements, que ce soit infrastructure ou applicatifs ; comment gérer le "day-2" ; comment gérer les évolutions de la plateforme ?

Nous vous proposons un REX sur l'ensemble de ces sujets, avec un beaucoup de tech, un peu d'organisation et un soupçon d'humour !

# Plan

## Génèse

Pourquoi migration Cloud ? Promesses attendues ?
Cloud au Centre, Facilité de déploiement, autonomie des équipes, ne pas être has been.
Expérimentation

## Disclaimer

Traits épaissis, MON expérience, ne pas tout prendre au pied de la lettre

## Présentation Entreprises

### IRSN

- Site irsn.fr -> Cloud
- Site xxx -> Cloud
- Site yyy -> Cloud

Etc.

### Accenture

## Organisation

IRSN : Chefferie de projet
Accenture : Gestion de la forge logicielle, Mise à dispo des plateformes et accompagnement migration
Autres : Développement des applications

### Worflow standard

-> Image wikijs de la forge
-> Schema exemple d'une appli

On reviendra plus tard sur les + et - de cette orga.

# 3 axes

- Gestion de la forge & migrations
- Déploiement des clusters applis & upgrades
- Accompagnement au déploiement & support, et plus ...

# La forge logicielle

Applications LL

Challenges
- Migration charts Helm -> à suivre
- Applications deviennent des SPOF

Cluster non managé:
- Pb RKE / CSI -> Migration cloud public
- Split des clusters Forge, Admin, Rancher
- Pb avec Rancher et ETCD

Anecdote: Migration Harbor qui s'est mal passée

Cycle d'upgrade des applis de la forge: ça dépend

Pb Harbor qui est sur le même cluster que les pods qui ont besoin de le contacter.

# Les clusters

## Schéma général

2 clusters : 1 non prod, 1 prod

Chez OVH

Aujourd'hui, 20 clusters, entre 3 et 10 noeuds, X pods, Y Applications

Déploiement de la stack "infra" : Ingress, Prom, Velero, Kyverno (ne pas afficher de suite)

1ere étape: déploiement à la main, click manager + chart helm
2e étape: TF, mais pb sur les maj & chart Helm, fausse bonne idée
3e etape : TF pour bootstrap, puis Charts Helm
GitOps: Argo/Flux ? Pas pour le moment, maturité de l'équipe

Pb rencontrés:
- provider terraform pas sec à l'époque
- Install d'un nouveau cluster : dernières versions infras applis, mais au final pas homogène
 -> Homogénéisation, mais demande de repasser sur les clusters

## Couche "infra"

Observabilité : on découvre des choses, gestion des logs
-> Schéma multi cluster

Backup : pareil, difficile de mettre en place, plein d'astuces (conf)
Anecdote PV non sauvegardé

Sécurité : kyverno

Etc..

Couche de plus en plus importante, de plus en plus de clusters, à surveiller: effet boule de neige

## Mises à jour

Upgrade version kube : pluto / kubent. Pb de trucs obsoletes (ingress ou cert manager par ex)

Lorsqu'on met à jour -> aussi la partie "infra", mais toutes les mêmes versions pour une version kube
-> Attention à la partie CI/CD avec kubectl !

Charts applicatifs obsolètes : qui gère ? Applications déployées, mais y a plus de devs sur le projet...

Pb de séquence : Versions de kube OVH vs version supporté par Rancher (soucis lors de rancher sur vieux cluster non managé)

Mise à jour Helm : svc qui dispparaissent !

### PRA

S'assurer que la version de k8s sur le PRA soit dispo ^^

## Sécurité

Difficile de restreindre les droits
- Droits CI/CD
- Droits Rancher
- Kyverno (ex network policies) -> Quand ç aplante ça marche plus

Au début : openbar
Restrictions au fur & à mesure:
 - SSO déjà en place, outil gérant les droits
 - droits d'accès sur le cluster : whitelist API Server, utilisateurs, CI, 
 - Gestion des secrets : GitLab avec SOPS, Vault ?
 - CI/CD avec Trivy -> bypass ?
 - Run avec Kyverno & Trivy-Operator

Bug Bounty sur la forge logicielle : nous a obligé à renforcer tout cela

Pb SOC: syslog sur VM

## Coûts

De plus en plus de machines / clusters -> instance à l'heure / mois -> exporter prometheus

# Accompagnement

Dev d'applis via appel d'offre, certains contrat avec du support, mais durée limitée dans le temps.

Disparité des applis (Java springboot, JS, Python, PHP, etc.) -> Préconisations, mais pas d'obligations.
-> Obligé de faire avec, et apprendre les spécificités de chacun.

Lien direct avec les équipes, même si entreprises !=. Système de ticket avec GitLab -> vieille habitude du mail

Aides sur plusieurs points:
- Mise en place des environnements : Cluster k8s + espace GitLab
- Mise en place CI/CD pour le déploiement des applis
- Aide & support au déploiement
- Analyse avant MEP si tout est OK
- Support pour analyses de problèmes côté infra

## Maturité des équipes

3 catégories:
- Autonomes
- Intermédiaires, besoin d'encadrement
- Débutante, faut (presque) tout faire ;)

Un dev n'est pas un expert CI/CD ou déploiement chart Helm.

## Qq fails (bouh les devs)

Ex: Docker Apache

Ex pb de migration BDD avec Flyway
 - Avec INSERT OR UPDATE
 - Avec Harbor (script au démarrage)

Ex Charts Helm avec Ingress, Certificats, PVC, etc.

Pb certificat Let's Encrypt vieilles images

Versions d'Ubuntu

Log4J (lien article)

FTP qui n'a jamais marché

BDD & disque local + golden image

## CI/CD

- Templates -> R2Devops ?
- Difficile de gouverner

Déploiement avec des charts Helm.

## Positionnement

Flou entre le build & le run

# En Prod

Accès aux logs applis ?

Debug ?

Requete & Dump SQL de la BDD, etc : LB, kubectl exec, etc.

Pb de Proxy : mise en place d'un bastion

Mise en place de runbooks

Scaling : scale 0 des nodes : mauvaise idée ;)

# La suite

Nouvelles applications à déployer
FinOps : Surement mutualiser quelques clusters, par niveau de dispo (pour la partie astreinte / MCO)
Flux vs Argo : pour faciliter le (re)déploiement des briques infra (stop au kubectl foreach)

# Conclusion

Promesses tenues ?
Autonomie des devs / déploiements ?
Si c'était à refaire ?
