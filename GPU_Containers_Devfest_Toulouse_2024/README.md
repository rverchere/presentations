---
marp: true
theme: devfest
markdown.marp.enableHtml: true
header: 'DevFest Toulouse 2024'
---
<!--
Abstract:
  Après avoir validé le POC du dernier projet IA, à grands coups de requêtes vers OpenAI, la DSI met le holà, impossible d’envoyer des informations de l’entreprise à un service tiers, on va gérer nos LLMs sur nos propres clusters Kubernetes !
  
  Cela demande par contre d’avoir des GPUs (sic) pour que ce soit performant, accessibles aux applications conteneurisées, mais alors comment ça marche ?! Et puis les GPUs c’est cher, c’est rare, comment les utiliser au mieux sans exploser les budgets ?

  Je vous propose alors de voir ensemble comment, grâce à l’opérateur “NVIDIA GPU Operator“ on peut accéder à ces fameux GPUs : installation, configuration, interaction avec l’hôte et gestion des modules noyau, mais surtout les contraintes et divers modes de partage de ressources (time-slicing, mig), et d’autres add-ons sympa comme le “node-feature-discovery” pour utiliser au mieux les ressources, le tout en mode pas-à-pas.

  Après cette session, mes équipes de devs pourront enfin avoir du GPU dans leurs conteneurs
-->
<!--
_transition: slide
_class: first
-->

![center width:256px](assets/logo_devfest_toulouse.png)

# ✨ Du GPU dans mes conteneurs !

## Rémi Verchère @ Accenture

---
<!--
_footer: https://presentations.verchere.fr/GPU_Containers_Devfest_Toulouse_2024/
-->
![bg fit right:35%](assets/photo-profil.png)
![bg fit right:35% 70%](assets/Acc_GT_Dimensional_RGB.svg)

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

👷 Ops qui aide les Devs à mettre en prod

🫶 Open Source & CNCF landscape

☸️ Kubestronaut

👨‍🍼 Papa x 👦👦👧👧

🌐 *@rverchere*

⚠️ *Présentation non sponsorisée par NVIDIA*

<!--
---
# Mon application a besoin de GPU !

![bg right:35%](assets/question-repondue.png)


## Oui, mais pourquoi ?
- Démocratisation de la  **GenAI** ✨
- Quand le CPU ne suffit plus 🖥️
- Dev. des outils ~~inutiles & énergivores~~ cools 🤭
- Générer des articles techs ou abstracts CFP 🤭

## Problématique
- Plus de crédits vers OpenAI
- Besoin de contrôler les données générées
- ➡️ Comment on gère sur nos clusters K8S ?
-->

<!--
---
# Mise en situation

> L'entreprise "**Massil.IA**" développe des applications ultra tendance. Ce sont pour l'instant des 'POCs' qui font des calls API vers *OpenAI*.
La DSI souhaite arrêter l'hémoragie des tokens, maitriser les données générées, et oblige à installer de cartes GPU sur les machines au sein du cluster Kubernetes où sont hébergées les applications.

&nbsp;

➡️ Comment on gère sur nos clusters K8S
l'accès aux GPUs ?

![bg fill right:33%](assets/gpu-marseille.png)
-->

---
<!--
_transition: slide
-->
# Agenda

## Sujets traités

- Gestion GPU côté **dev**
- Gestion GPU côté **ops**  💪
  - Tour du propriétaire **NVIDIA GPU Operator**
  - Gestion des nodes & méthodes d'accès au GPU
- 👨‍🏫 Le tout avec des démos quand même (avec du `YAML` ^^)

## Sujets non traités

- Quel GPU, Cloud Provider, Distribution K8S OnPrem choisir
- Quels modèles de diffusion utiliser, génération de prompts efficaces, etc.

---
<!--
_class: lead
-->
# Côté **dev** 


---
# Activation du GPU côté dev

## Easy 😎 !


```python
from diffusers import StableDiffusion3Pipeline
import torch

pipe_id = "stabilityai/stable-diffusion-3-medium-diffuser"
pipe = StableDiffusion3Pipeline.from_pretrained(pipe_id,
        torch_dtype=torch.float16)
# [...]
```
```python
pipe.to("cuda") # <-- Et voilà !
```

```python
# [...]
```

---
<!--
_transition: slide
-->
# Activation du GPU côté dev

## Easy 😎 !


```python
from diffusers import StableDiffusion3Pipeline
import torch

pipe_id = "stabilityai/stable-diffusion-3-medium-diffuser"
pipe = StableDiffusion3Pipeline.from_pretrained(pipe_id,
        torch_dtype=torch.float16)
# [...]
pipe.to("cuda") # <-- Et voilà !
# [...]
```

## Environnement de démo

- Apps Python,  Streamlit + Pytorch + Diffusers + StableDiffusion 🐍

<!--
Compute Unified Device Architecture
-->

---
<!--
_class: lead
-->
# Côté **ops** 🧐

---
# Cluster Kubernetes

## Besoins

- 1 Cluster Kubernetes (of course)
- 1 ou plusieurs nodes avec carte GPU

![bg fill right:42%](assets/gpu-marseille.png)

---
<!--
_transition: star
-->
# Cluster Kubernetes

## Besoins

- 1 Cluster Kubernetes (of course)
- 1 ou plusieurs nodes avec carte GPU

![bg fill right:42%](assets/gpu-marseille.png)

## Environnement de démo

- Cluster OVHCloud MKS, v1.31 (🎵)
- Nodes avec 1 GPU "H100" 💸

<!--
On verra plus tard comment tout cela s'installe sur les GAFAMs
-->

---
# Démo time !

&nbsp;

![center width:800px](assets/demo.gif)

---
# Cluster Kubernetes

## Besoins

- 1 Cluster (of course)
- 1 ou plusieurs nodes avec une carte GPU

![bg right:33% 150%](assets/nvidia-gpu-operator-image.jpg)


---
# Cluster Kubernetes

## Besoins

- 1 Cluster (of course)
- 1 ou plusieurs nodes avec une carte GPU
- **✨ Le fameux opérateur *"NVIDIA GPU Operator"***

![bg right:33% 150%](assets/nvidia-gpu-operator-image.jpg)

---
<!--
_transition: slide
-->

# Activation du GPU côté DevOoops

## Déploiement sur Kubernetes

<!--
- CPU
- Memory
- GPU ??
-->

```yaml
apiVersion: apps/v1
kind: Deployment
[..]
    spec:
      runtimeClassName: nvidia # <-- 🤔
      containers:
      - name: my-container-using-gpu
        resources:
          limits:
            nvidia.com/gpu: "1" # <-- 🤔
      nodeSelector:
        nvidia.com/gpu.family: ampere # <- 🤔
```

---
<!--
_class: lead
-->
# NVIDIA GPU Operator

---
<!--
_footer: https://developer.nvidia.com/blog/improving-gpu-utilization-in-kubernetes
-->
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
* NVIDIA GPU Operator, *CQFD*
* NVIDIA GPU Driver
* NVIDIA Container Toolkit
* NVIDIA Kubernetes Device Plugin
* NVIDIA GPU Feature Discovery
* NVIDIA MIG Manager
* NVIDIA DCGM Exporter
* D'autres trucs qu'on aura pas le temps de traiter (KubeVirt, Confidential Computing, Kata, etc.)

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
<!--
_footer: https://www.jimangel.io/posts/nvidia-rtx-gpu-kubernetes-setup
-->
# NVIDIA GPU Operator

![bg right:35% 90%](assets/NV-GPU-Operator-1.png)

![center width:380](assets/gpu-stack-full.jpg)

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- **NVIDIA GPU Operator**
- NVIDIA GPU Driver
- NVIDIA Container Toolkit
- NVIDIA Kubernetes Device Plugin
- NVIDIA GPU Feature Discovery
- NVIDIA MIG Manager
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
# Composants : NVIDIA GPU Operator

![bg right:35% fit](assets/k8s-operator.png)

Custom Resource `nvidia.com/v1/clusterpolicies`

```yaml
apiVersion: nvidia.com/v1
kind: ClusterPolicy
metadata:
   name: cluster-policy
spec:
  driver:
    enabled: true
      hostPaths:
    driverInstallDir: /run/nvidia/driver
    rootFS: /
  migManager:
    config:
      default: all-disabled
      name: default-mig-parted-config
    enabled: true
  [...]
```

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- **NVIDIA GPU Driver**
- NVIDIA Container Toolkit
- NVIDIA Kubernetes Device Plugin
- NVIDIA GPU Feature Discovery
- NVIDIA MIG Manager
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
<!--
_footer: https://docs.nvidia.com/datacenter/cloud-native/gpu-operator/latest/gpu-driver-configuration.html
-->
# Composants : NVIDIA GPU Driver

- Si besoin, installe le driver NVIDIA (*optionnel*)

```
$ kubectl logs nvidia-driver-daemonset-zljb6
  The kernel was built by: gcc (Ubuntu 11.4.Cleaning kernel module build directory.
Building kernel modules:

  [##############################] 100%
Kernel module compilation complete.
Kernel messages:
[  405.347857] nvidia: loading out-of-tree module taints kernel.
[  405.347875] nvidia: module license 'NVIDIA' taints kernel.
[  405.347879] Disabling lock debugging due to kernel taint
[  405.361004] nvidia: module verification failed: signature and/or required key missing - tainting kernel
[  405.385913] nvidia-nvlink: Nvlink Core is being initialized, major device number 235
[  405.385920] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  550.90.07  Fri May 31 09:35:42 UTC 2024
[  405.458423] nvidia_uvm: module uses symbols from proprietary module nvidia, inheriting taint.
[  405.464070] nvidia-uvm: Loaded the UVM driver, major device number 511.
[  405.469814] nvidia-modeset: Loading NVIDIA Kernel Mode Setting Driver for UNIX platforms  550.90.07  Fri May 31 09:30:47 UTC 2024
[  405.476414] nvidia-modeset: Unloading
[  405.523298] nvidia-uvm: Unloaded the UVM driver.
[  405.564398] nvidia-nvlink: Unregistered Nvlink Core, major device number 235
Installing 'NVIDIA Accelerated Graphics Driver for Linux-x86_64' (550.90.07):: Installing

```

---
# Composants : NVIDIA GPU Driver

## Old Good Times 🧓

![center width:720](assets/Linux_4.4.2_ncurses_configuration.png)

---
<!--
_footer: https://docs.nvidia.com/datacenter/cloud-native/gpu-operator/latest/gpu-driver-configuration.html
-->
# Composants : NVIDIA GPU Driver

- Version actuelle `550.90.07`, défini dans la CRD `ClusterPolicy` de l'opérateur
- Possibilité d'utiliser des drivers pré-compilés (**Technology Preview**)
- Possibilité d'avoir plusieurs drivers (**Technology Preview**), avec CRD `NVIDIADriver`

![center width:400](assets/nvd-basics.svg)

- 📺 [Enable GPU-Acceleration Without Worrying About Managing Device Drivers](https://youtu.be/zYZ0zXWDH00)

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- NVIDIA GPU Driver
- **NVIDIA Container Toolkit**
- NVIDIA Kubernetes Device Plugin
- NVIDIA GPU Feature Discovery
- NVIDIA MIG Manager
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
<!--
_footer: https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/latest/index.html
-->
# Composants : NVIDIA Container Toolkit

![bg right:30% 95%](assets/nvidia-container-toolkit.png)

- Installation du **container runtime** pour permettre l'accès au GPU depuis les conteneurs.
- Patch de `/etc/containerd/config.toml`
  ```toml
  [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.nvidia.options]
    BinaryName = "/usr/local/nvidia/toolkit/nvidia-container-runtime"
    SystemdCgroup = true
    imports = ["/etc/containerd/default.toml"]
  ```
- Définition d'un **runtimeClass** `nvidia`
  ```
  kubectl describe runtimeclass nvidia
  Name:         nvidia
  Namespace:
  Labels:       app.kubernetes.io/component=gpu-operator
  API Version:  node.k8s.io/v1
  Handler:      nvidia
  Kind:         RuntimeClass
  ```

---
# Composants : NVIDIA Container Toolkit

![center width:1000](assets/gpu-device-plugin-hooks.png)

---
# Composants : NVIDIA Container Toolkit

- *Optionnel* (sinon doit être déjà installé sur le node)
- runtimeClass *par défaut* sur le node GPU

```yaml
apiVersion: apps/v1
kind: Deployment
[..]
    spec:
      runtimeClassName: nvidia # <-- ✅ NVIDIA Container Toolkit (*par défaut*)
      containers:
      - name: my-container-using-gpu
```

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- NVIDIA GPU Driver
- NVIDIA Container Toolkit
- **NVIDIA Kubernetes Device Plugin**
- NVIDIA GPU Feature Discovery
- NVIDIA MIG Manager
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
#  Composants : NVIDIA Kubernetes Device Plugin

- Exposition automatique du nombre de GPUs de chaque node
  ```yaml
  # kubectl get node gpu-l4-node-5430b1 -o yaml
  status:
    capacity:                  
      nvidia.com/gpu: "4"
    allocatable:               
      nvidia.com/gpu: "4"
  ```
- Suivi de l'état de santé (*health*) des GPUs

- Partage de ressources GPU entre pods (GPU Sharing 🎵)

- ➡️ Voir concepts Kubernetes [Device Plugins](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/)

---
#  Composants : NVIDIA Kubernetes Device Plugin

![center width:1000](assets/gpu-device-plugin-workflow.png)

---
#  Composants : NVIDIA Kubernetes Device Plugin

- Permet d'informer le 'scheduler' quels pods placer sur des nodes avec GPU
  ```yaml
  apiVersion: apps/v1
  kind: Deployment
  [..]
      spec:
        runtimeClassName: nvidia # <-- ✅ NVIDIA Container Toolkit (par défaut)
        containers:
        - name: my-container-using-gpu
          resources:
            limits:
              nvidia.com/gpu: "1" # <-- ✅ NVIDIA Kubernetes Device Plugin
  ```
- 📺 [Sharing Is Caring: GPU Sharing and CDI in Device Plugins](https://www.youtube.com/watch?v=Q2GuTUO170w)

- 💡 Pour aller plus loin : **Container Device Interface (CDI)**


---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- NVIDIA GPU Driver
- NVIDIA Container Toolkit
- NVIDIA Kubernetes Device Plugin
- **NVIDIA GPU Feature Discovery**
- NVIDIA MIG Manager
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
# Composants : NVIDIA GPU Feature Discovery

- Ajout de labels *GPU Feature Discovery* (part of Kubernetes Device Plugin)

```yaml
# kubectl get nodes gpu-l4-node-5430b1 -o yaml
metadata:
  labels:
    nvidia.com/cuda.driver-version.full: 550.90.07
    nvidia.com/cuda.runtime-version.full: "12.4"
    nvidia.com/gpu.count: "1"
    nvidia.com/gpu.deploy.container-toolkit: "true"
    nvidia.com/gpu.deploy.device-plugin: "true"
    nvidia.com/gpu.deploy.driver: "true"
    nvidia.com/gpu.deploy.gpu-feature-discovery: "true"
    nvidia.com/gpu.family: ampere
    nvidia.com/gpu.memory: "23034"
    nvidia.com/gpu.product: NVIDIA-L4
    nvidia.com/mig.capable: "false"
    nvidia.com/mig.strategy: single
    [...]
```

---
# Composant bonus : Node Feature Discovery

- 💡 Pas spécifique à NVIDIA, mais embarqué avec

```yaml
# kubectl get nodes gpu-l4-node-5430b1 -o yaml 
metadata:
  labels:    
    feature.node.kubernetes.io/cpu-cpuid.ADX: "true"
    feature.node.kubernetes.io/cpu-hardware_multithreading: "false"
    feature.node.kubernetes.io/cpu-model.family: "25"
    feature.node.kubernetes.io/cpu-model.id: "17"
    feature.node.kubernetes.io/cpu-model.vendor_id: AMD
    feature.node.kubernetes.io/kernel-version.full: 5.15.0-121-generic
    feature.node.kubernetes.io/system-os_release.ID: ubuntu
    feature.node.kubernetes.io/system-os_release.VERSION_ID: "22.04"
    feature.node.kubernetes.io/system-os_release.VERSION_ID.major: "22"
    feature.node.kubernetes.io/system-os_release.VERSION_ID.minor: "04"
  ```
- ➡️ Voir [node-feature-discovery](https://github.com/kubernetes-sigs/node-feature-discovery)

---
# Exemple de déploiement conteneur GPU classique

## my-app-using-gpu.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
[..]
    spec:
      runtimeClassName: nvidia # <-- ✅ NVIDIA Container Toolkit (par défaut)
      containers:
      - name: my-container-using-gpu
        [...]
        resources:
          limits:
            nvidia.com/gpu: "1" # <-- ✅ NVIDIA Kubernetes Device Plugin
      nodeSelector:
        nvidia.com/gpu.family: ampere # <-- ✅ NVIDIA GPU Feature Discovery
        feature.node.kubernetes.io/cpu-model.vendor_id: AMD # <-- ✅ Node Feature Discovery
```

---
<!--
_transition: star
-->
# Composants pour déployer une conteneur avec accès au GPU 🤯

```
❯ kubectl get pods
NAME                                                         READY   STATUS      RESTARTS      AGE
gpu-feature-discovery-8m6tj                                  1/1     Running     0             98m
gpu-operator-587db77bb5-zzrq7                                1/1     Running     4 (17h ago)   2d
gpu-operator-node-feature-discovery-gc-fc549bd94-9zsgc       1/1     Running     0             2d
gpu-operator-node-feature-discovery-master-b4bb855b7-s854s   1/1     Running     0             2d
gpu-operator-node-feature-discovery-worker-4p5mv             1/1     Running     0             2d
gpu-operator-node-feature-discovery-worker-6pbxt             1/1     Running     0             103m
gpu-operator-node-feature-discovery-worker-pp5kw             1/1     Running     4 (23h ago)   2d
nvidia-container-toolkit-daemonset-bggm4                     1/1     Running     0             98m
nvidia-cuda-validator-j9ps9                                  0/1     Completed   0             95m
nvidia-dcgm-exporter-gwd9k                                   1/1     Running     0             98m
nvidia-device-plugin-daemonset-zz7nw                         1/1     Running     0             98m
nvidia-driver-daemonset-zljb6                                1/1     Running     0             98m
nvidia-operator-validator-cb5px                              1/1     Running     0             98m
```
---
<!--
_transition: slide
-->
# Démo time !

&nbsp;

![center width:800px](assets/demo.gif)

---
<!--
_class: lead
-->
# Quand j'ai plus de 2 applications
# Sur mon cluster...

---
<!--
_footer: https://developer.nvidia.com/blog/improving-gpu-utilization-in-kubernetes
-->
# GPU Sharing

![](assets/GPU-Concurrency-Mechanisms.png)

---
<!--
_footer: https://kubernetes.web.cern.ch/blog/2023/01/09/efficient-access-to-shared-gpu-resources-part-1/
-->
# Stratégie 1 : Time Slicing

- Découpage du GPU par slots de temps ("Round-Robin Scheduling")

&nbsp;

![center ](assets/time-slicing.png)

---
# Stratégie 1 : Time Slicing

```yaml
# Values chart helm -> ConfigMap
devicePlugin:
  config:
    create: true
    default: "ts4"
    name: timeslicing-parted-config
    data:
      ts4: |-
        version: v1
        sharing:
          timeSlicing:
            resources:
              - name: nvidia.com/gpu
                replicas: 4
            renameByDefault: false # Pour que ce soit transparent pour l'appli
            failRequestsGreaterThanOne: false
```
---
# Stratégie 1 : Time Slicing

```yaml
# kubectl describe node
labels:
  nvidia.com/gpu.product=NVIDIA-H100-SHARED
  nvidia.com/gpu.replicas=4
Capacity:                  
  nvidia.com/gpu:         4
Allocatable:               
  nvidia.com/gpu:         4
```

---
# Stratégie 1 : Time Slicing

## GPU Memory 😨

```
# nvidia-smi
+-----------------------------------------------------------------------------------------+
| Processes:                                                                              |
|  GPU   GI   CI        PID   Type   Process name                              GPU Memory |
|        ID   ID                                                               Usage      |
|=========================================================================================|
|    0   N/A  N/A    119580      C   /opt/venv/bin/python                         5236MiB |
|    0   N/A  N/A    119647      C   /opt/venv/bin/python                        17170MiB |
+-----------------------------------------------------------------------------------------+
```

![center width:600](assets/timesplit-error-memory.png)

---
# Stratégie 2 : Multi-Process Service (MPS)

- Partitionnement logique (***experimental***)
- Géré par un serveur "MPS" sur le host

![center](assets/sharing-mps.png)

---
# Stratégie 2 : Multi-Process Service (MPS)

```yaml
# Values chart helm -> ConfigMap
devicePlugin:
  config:
    create: true
    default: "mps4"
    name: "mps-parted-config"
    data:
      mps4: |-
        version: v1
        sharing:
          mps:
            resources:
            - name: nvidia.com/gpu
              replicas: 4
```

---
<!--
_footer: https://docs.nvidia.com/datacenter/cloud-native/gpu-operator/latest/gpu-operator-mig.html , https://docs.nvidia.com/datacenter/tesla/mig-user-guide/index.html
-->
# Stratégie 3 : Multi-Instance GPUs (MIG)

- Partitionnement hardware en "mini-GPUs"
- Isolation complète entre les applications
- Plusieurs profils de "mini-GPUs"

![center width:700](assets/gpu-instances-example.png)

- 💸 Disponible que sur modèles Ampere A100/H100 et plus
- Composants: "NVIDIA MIG Manager" pour paramétrer les GPUs automatiquement 

---
# Stratégie 3 : Multi-Instance GPUs (MIG)

```yaml
# Values chart helm 
mig:
  strategy: mixed # ou single
```

```yaml
mig-configs:
  # A100-40GB, A800-40GB
  all-2g.10gb:
    - devices: all
      mig-enabled: true
      mig-devices:
        "2g.10gb": 3
  all-4g.20gb:
    - devices: all
      mig-enabled: true
      mig-devices:
        "4g.20gb": 1
```
![bg right:35% 95%](assets/mig-partitioning-ex3.png)

<!--
Single vs Mixed: 1 ou plusieurs types de GPU par node
-->

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- NVIDIA GPU Driver
- NVIDIA Container Toolkit
- NVIDIA Kubernetes Device Plugin
- NVIDIA GPU Feature Discovery
- **NVIDIA MIG Manager**
- NVIDIA DCGM Exporter

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
<!--
_transition: star
_footer: https://developer.nvidia.com/blog/improving-gpu-utilization-in-kubernetes/
-->

# Quelle stratégie ?

> Well, it depends...

&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;
&nbsp;

*💡 Pour aller plus loin*
📺 [Dynamic Ressource Alloc.](https://www.youtube.com/watch?v=u7CktFEJbS8)
  
![bg right:57% fit](assets/sharing-strategies.png)

---
<!--
_transition: slide
-->
# Demo time !

&nbsp;

![center width:800px](assets/demo.gif)

---
<!--
_class: lead
_backgroundColor: #34a853
-->
# "Une appli non monitorée, ce n'est pas une application en prod"

---
# NVIDIA GPU Operator

Opérateur Kubernetes, plusieurs composants :
- NVIDIA GPU Operator
- NVIDIA GPU Driver
- NVIDIA Container Toolkit
- NVIDIA Kubernetes Device Plugin
- NVIDIA GPU Feature Discovery
- NVIDIA MIG Manager
- **NVIDIA DCGM Exporter**

![bg right:35% 175%](assets/GPU-K8-featured.png)

---
<!--
_transition: slide
_footer: https://grafana.com/grafana/dashboards/12239-nvidia-dcgm-exporter-dashboard/
-->
# Composants: NVIDIA DCGM Exporter (Data Center GPU Manager)

- Exemple live
  - https://nvidia-dcgm-exporter.devfest-toulouse.opsrel.io/metrics
  - https://grafana.devfest-toulouse.opsrel.io

![center width:800](assets/grafana-dcgm.png)

---
<!--
_class: lead
_backgroundColor: #8ab4f8
-->
# Point Bonus
# Déploiement sur les autres Cloud Providers

---
<!--
_footer: https://cloud.google.com/kubernetes-engine/docs/concepts/timesharing-gpus?hl=fr
-->
# GCP

## Exemple: timeSlicing sur GKE

```sh
gcloud container clusters create CLUSTER_NAME \
    --region=COMPUTE_REGION \
    --cluster-version=CLUSTER_VERSION \
    --machine-type=MACHINE_TYPE \
    # Tout sur la même ligne, ici c'est pour la visibilité
    --accelerator=type=GPU_TYPE,\
        count=GPU_QUANTITY,\
        gpu-sharing-strategy=time-sharing,\
        max-shared-clients-per-gpu=CLIENTS_PER_GPU,\
        gpu-driver-version=DRIVER_VERSION
```
---
<!--
_transition: slide
-->
# Amazon EKS
1. Installation d'un cluster avec [images AMI optimisées GPU](https://aws.amazon.com/fr/ec2/instance-types/#Accelerated_Computing)
2. Installation d'au moins du "NVIDIA device plugin" (avec ou sans l'opérateur)

# Azure AKS
1. Installation d'un cluster avec un nodepool [images AMI optimisées GPU](https://aws.amazon.com/fr/ec2/instance-types/#Accelerated_Computing)
2. Installation d'au moins du "NVIDIA device plugin" (avec ou sans l'opérateur)
3. NVIDIA conseille même l'option `--skip-gpu-driver-install`
4. ⚠️ "*AKS GPU image (preview)*" avec tout dedans est dépréciée 🤔

# Scaleway
1. Opérateur automatiquement déployé avec les nodepools GPUs 👏

---
<!--
_transition: star
-->
# Ressources

- Slides : https://presentations.verchere.fr/GPU_Containers_Devfest_Toulouse_2024
- Code & scripts démo : https://github.com/rverchere/gpu-k8s-demo
&nbsp;
- https://www.jimangel.io/posts/nvidia-rtx-gpu-kubernetes-setup
- https://ronanquigley.com/blog/understanding-gpu-sharing-strategies-in-kubernetes
- https://kubernetes.web.cern.ch/blog/2023/01/09/efficient-access-to-shared-gpu-resources-part-1/
- https://superorbital.io/blog/gpu-kubernetes-nvidia-device-plugin/
- [Playlist Youtube](https://www.youtube.com/watch?v=8JGduez-8S0&list=PLA7f-vLErEIIKTtmRCm-i45I33d6ObOZE) 
- Et plein d'autres...

---
<!--
_class: last
-->

![width:250](assets/openfeedback.png)

# Merci !

## Rémi Verchère @ Accenture

![bg](assets/sanglier-chocolatine.webp)
