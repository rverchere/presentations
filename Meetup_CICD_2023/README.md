---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<style>
section {
  background-color: #fff;
  background-image: none;
  color: #000; 
  font-family: 'Source Sans Pro';
  font-size: 28px;
}
section.lead {
  background-color: #222;
  background-size: cover;
  color: #fff;
}

section.lead h1 {
  color: #fff;
}
section.lead h2 {
  color: #fff;
}
section.lead a {
  color: #ccc;
}
h1 {
  color: #000;
}
h1 strong{
  color: #000;
}
h2 {
  color: #666;
}
h2 strong{
  color: #666;
}
h3 {
  color: #aaa;
}
h3 strong{
  color: #aaa;
}
a {
  color: #222;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
table {
    width: 100%;
}
</style>

<!--
_class: lead
-->

# La CI/CD facile (sur GitLab) avec
# R2devops et to-be-continuous
*Rémi Verchère @ Accenture*

---
## Au sommaire
- Rappel de quelques concepts CI/CD & GitLab-CI
- Présentation de R2Devops + démo
- Présentation de to-be-continuous + démo
- Et la surprise du chef !

## La démo
- Application Python Flask
- Création d'image Docker
- Déploiement sur Kubernetes
- https://gitlab.com/rverchere/k8s-app-flask
---
<!--
_transition: flip
-->

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

- Ops qui aime bien les Devs

- Papa x 👦👦👧👧

- GitLab Certified Associate

- 🌐 *@rverchere*

- 📖 https://presentations.verchere.fr/

![bg fit right:40%](assets/photo-profil.png)
![bg fit right:40% 70%](assets/Acc_GT_Dimensional_RGB.svg)

---
<!--
_class: lead
-->

# CI / CD

---
# CI / CD

> *Wikipedia*: En génie logiciel, CI/CD (parfois écrit CICD) est la combinaison des pratiques d'**intégration continue** et de **livraison continue** ou de **déploiement continu**.

> *Le CI/CD comble le fossé entre les activités et les équipes de développement et d'exploitation en imposant l'automatisation de la création, des tests et du déploiement des applications. Les pratiques DevOps modernes impliquent le développement continu, le test continu, l'intégration continue, le déploiement continu et la surveillance continue des applications logicielles tout au long de leur cycle de vie. La pratique CI/CD, ou pipeline CI/CD, constitue l'épine dorsale des opérations DevOps modernes.*

---
![bg 80%](assets/gitlab_workflow_example_extended_v12_3.png)

---
# GitLab-CI

- Solution de CI/CD proposée par GitLab
- Fichier `.gitlab-ci.yml` qui définit les actions de CI/CD
- Runners qui les exécutent et renvoient le résultat au serveur GitLab

![center width:600px](assets/gitlab-runner-archi.png)

---
# GitLab-CI

Un peu de vocabulaire :
- Pipelines
- Stages
- Jobs

![center width:1100px](assets/example-pipeline-1.png)

---
# CI / CD

## Mise en oeuvre

- Quels stages ?

- Quels jobs, et on met quoi exactement dans les jobs ?

- Quel environnement d'exécution pour les jobs ?

- Workflow Git, mais pas de worklow CI/CD ?

---
<!--
_transition: flip
-->

![bg fit right:40%](assets/twitter-ci.png)
# CI / CD

## Mise à l'échelle

- Comment partager (`templates`) ?

- Comment rechercher ?

- Comment versionner ?

- Comment rester "simple" (`anchors`, ...) ?
  https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/.gitlab-ci.yml

---
<!--
_class: lead
-->

# R2Devops

---
# R2Devops

- **Marketplace** de jobs de CI/CD, pour améliorer la vie des devs (*qui ont autre chose à faire que d'écrire des pipelines de CI/CD*), **éditeur** intégré 

- Créé en 2020 par la société GO2scale @ Montpellier.

- **Catalogue** de jobs officiels, communautaires, partenaires, public ou privé.

- **Plateforme** Saas ou On Premise

- Pour aller plus loin
  - Site Web : https://r2devops.io/
  - Serveur Discord : https://discord.gg/FmxtX8CWBH
  - Comment concilier CI/CD et Developer Experience ? https://youtu.be/tOtAFoDY6BM

---
<!--
_transition: flip
-->

# R2Devops

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

# To Be Continuous

---
# To Be Continuous

- **Templates** de **pipelines complets**, accessibles à tout le monde

- Projet initié chez Orange en 2019, "Open sourcé" en 2021

- **Standardisation** complète, *opiniated* workflows, stages.

- Configurateur en ligne **kicker**

- Pour aller plus loin
  - Site Web : https://to-be-continuous.gitlab.io/
  - Serveur Discord : https://discord.gg/c9qnAXRssn
  - « to be continuous », mon pipeline GitLab CI en quelques minutes:  https://youtu.be/LOWnPgWXXjU

---
<!--
_transition: flip
-->

# To Be Continuous

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

# La Surprise du Chef !

---
# R2devops + to-be-continuous ??

- R2devops: Developer Experience, Marketplace, facilité et 
- to-be-continuous: Pipelines complets, Workflows

# Collaboration !

- 🫶  R2Devops and to be continuous: collaborating to improve Open-Source CI/CD (24 avril 2023)!
https://blog.r2devops.io/blog/All/r2devops_and_to_be_continuous/

---
<!--
_transition: flip
-->

# R2devops + To Be Continuous

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

# Mon feedback, vos questions & merci !

*Rémi Verchère @ Accenture*
