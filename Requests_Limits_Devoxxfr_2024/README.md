---
marp: true
theme: devoxxfr
markdown.marp.enableHtml: true
header: 'Devox<span>x</span>fr 2024'
---
<!--
Lorsqu'on doit déployer une application sur un cluster Kubernetes, une bonne pratique est de définir des ressources "requests" et "limits" pour garantir le bon fonctionnement de celle-ci tout en garantissant la bonne santé du cluster qui l'accueille.

OK, mais quelles valeurs de "requests" et "limits" doit-on spécifier ? Pas assez de RAM et l'application sera "OOMKilled" ? Trop de CPU mais cela bloquera les autres déploiements ?

Après un tour d'horizon des différentes options pour paramétrer ces ressources, je vous présenterai un outil simple mais efficace pour vous aider à définir des valeurs pragmatiques : Goldilocks et l'utilisation automatique des Vertical Pod Autoscaler.
-->

<!--
_class: first
-->

## Build & Deploy

# Dimensionnez correctement vos déploiements Kubernetes

### RÉMI VERCHÈRE <br/> <span>ACCENTURE</span>

---
# Répondre à la question ⤵️

> **Quelles Requests & Limits définir sur vos applications conteneurisées ?**

# Agenda

- Rappel **Requests & Limits** dans **Kubernetes**
- Présentation de **2 outils (+ démo)** pour vous aider à les définir
- **Stratégies** de mise à l'échelle : focus sur **Vertical Pod Autoscaler**
- Q/A si on a du temps

<!--
_footer: Crédits : beaucoup d’images de chez Sysdig, Datadog & Robusta qui expliquent bien les choses.
-->

---
<!--
_transition: slide
_footer: https://presentations.verchere.fr/Requests_Limits_Devoxxfr_2024/
-->

![bg fit right:35%](assets/photo-profil.png)
![bg fit right:35% 70%](assets/Acc_GT_Dimensional_RGB.svg)

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

👷 Ops qui aide les Devs à mettre en prod

🫶 Open Source & CNCF landscape

👨‍🍼 Papa x 👦👦👧👧

🌐 *@rverchere*


---
<!--
_class: lead
-->

# Rappel **R**equests & **L**imits

---
# Requests & Limits

## Requests
Ressources nécessaires pour placer un conteneur sur un noeud.

## Limits
Ressources maximum qu'un conteneur peut utiliser sur un noeud.

## Sur **Memory** & **CPU** (📺)

![bg right:35% 80%](assets/deployment-example.png)

<!--
The Miserable Life of a CPU Instruction - Samuel Ortiz
https://www.youtube.com/watch?v=j6-yGGBTXGU
-->

---
# Requests & Limits

## Qualité de Service

- Ni Requests, ni Limits : **Best Effort**
- Requests < Limits : **Burstable**
- Requests = Limits : **Guaranteed**

## PDB, Evictions, Priority Classes

- Ce n'est pas le sujet aujourd'hui...


![bg right:36% fit](assets/14fig04_alt.jpg)

<!--
_footer: https://livebook.manning.com/concept/kubernetes/qos-class
-->

---
![bg 90%](assets/kubernetes-resources-cheatsheet-1170x585.png)

<!--
_footer: Aide-mémoire : https://sysdig.com/blog/kubernetes-limits-requests/
-->

---
# Requests & Limits

Quel sizing alors ? Trop ? Trop peu ?

![width:600px](assets/Millions-wasted-image2-1170x829.png)

![bg right:40% fit](assets/Millions-wasted-image1-768x805.png)

<!--
_footer: https://sysdig.com/blog/millions-wasted-kubernetes
-->

---
# Requests & Limits

![center width:950px](assets/2023-container-report-graphics-fact-5.png)

<!--
_footer: https://www.datadoghq.com/container-report/#5
-->

---
# Requests & Limits

![center width:950px](assets/2023-container-report-graphics-fact-5a.png)

<!--
_footer: https://www.datadoghq.com/container-report/#5
-->

---
# Requests & Limits

![center width:900px](assets/2024-castai-report.png)


<!--
_footer: https://cast.ai/press-release/cast-ai-analysis-finds-only-13-percent-of-provisioned-cpus-and-20-percent-of-memory-is-utilized/
-->


---
<!--
_transition: slide
_footer: 💡 **Requests trop hautes, scheduler ne peut placer les pods, alors que le cluster est "large".**
-->
# Requests & Limits

## Ça n'arrive pas qu'aux autres...

![center width:1080px](assets/mail-requests.png)

---
<!--
_class: lead
-->

# Aide au dimensionnement

---
# Aide au dimensionnement

## Plusieurs méthodes

1) Au doigt mouillé itératif ☝️
2) Spécial devs Java ⤴️ 🧌
3) Grâce aux métriques 🔍 !

<!--
1. Standards : Prometheus, VictoriaMetrics, Datadog, etc.
2. Plus "avancées" : VPA
-->

---
<!--
_transition: slide
-->

# Aide au dimensionnement

## Plusieurs méthodes

1. Au doigt mouillé itératif ☝️
2. Spécial devs Java ⤴️ 🧌
3. Grâce aux métriques 🔍 !

<!--
1. Standards : Prometheus, VictoriaMetrics, Datadog, etc.
2. Plus "avancées" : VPA
-->

## Outils pour vous faciliter la vie
1. **KRR**
2. **Goldilocks**

---
# Krr

- From Robusta : https://github.com/robusta-dev/krr
- "Prometheus-based **K**ubernetes **R**esource **R**ecommendations"
- CLI, sans agent, basé sur métriques Prometheus & co, extensible
  *Possibilité run in-cluster périodique (notifs Slack) avec Robusta*
- Plugin k9s
- Calcul : 👀 https://github.com/robusta-dev/krr#algorithm

![center width:1200px](assets/krr_example.png)

---
<!--
_transition: slide
-->
# Krr

## Démo !

![center width:650px](assets/demo.gif)

<p align="center">(🙏 OVHCloud)</p>

---
<!--
_transition: slide
-->

# Goldilocks

- From Fairwinds : https://goldilocks.docs.fairwinds.com/ 
- "Get your resource requests 'Just Right'"
- **Automatise** les "**V**ertical **P**od **A**utoscalers" en mode recommendation (⁉️)
- Interface Web pour analyse des recommendations VPA

![center width:550px](assets/goldilocks.png)

---
<!--
_class: lead
-->

# **V**ertical **P**od **A**utoscaler

---
# VPA

- Adaptation automatique des Requests & Limits du pod

![center width:1100px](assets/vpa-diagram.png)

<!--
_footer: https://www.kubecost.com/kubernetes-autoscaling/kubernetes-vpa/
-->

---
# VPA

- 3 composants
  - **Recommender**
  - Updater
  - Admission Controller
- Plusieurs modes
  - Recreate
  - Initial
  - **Off**

![bg right:60% fit](assets/vpa-allocate-resources.png)

<!--
_footer: https://www.kubecost.com/kubernetes-autoscaling/kubernetes-vpa/
-->

---
# VPA

## Remarques

- Pas par défaut, à **installer**
- Besoin de créer une **ressource** de type “VerticalPodAutoscaler”
- ⛔ A ne pas utiliser avec HPA
- **Restart** du conteneur ★
- **Calcul** : 👀 [vertical-pod-autoscaler.md#recommendation-model](https://github.com/kubernetes/design-proposals-archive/blob/main/autoscaling/vertical-pod-autoscaler.md#recommendation-model) 🤯
   - Requests : CPU 90% Percentile par défaut, RAM OOMKilled
   - Limits : Ratio Requests Initiaux
   - [FAQ - what-are-the-parameters-to-vpa-recommender](https://github.com/kubernetes/autoscaler/blob/master/vertical-pod-autoscaler/FAQ.md#what-are-the-parameters-to-vpa-recommender)

---
# VPA

## Exemple

```yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: devoxxfr-app
spec:
  targetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: devoxxfr-app
  updatePolicy:
    updateMode: "Off"
status:
  recommendation:
    containerRecommendations:
    - [...]
```

---
<!--
_transition: slide
-->
# Autres types d'Autoscalers

## 1) **H**orizontal **P**od **A**ustocaler
<!-- HPA: De plus en plus utilisé
(+55% des entreprises, rapport Sysdig) -->

![bg right:35% fit](assets/hpa-overview.png)

## 2) **C**luster **A**utoscaler
<!-- Coucou Karpenter ! -->

## 3) **M**ulti-dimensional **P**od **A**utoscaling
- AEP-5342 (Autoscaler Enhancement Proposal) Multi-dimensional Pod Autoscaler

---
# Goldilocks

## Démo !

![center width:650px](assets/demo.gif)

<p align="center">(🙏 OVHCloud)</p>

---
# Goldilocks

## C'est moche hein ?

![center width:780px](assets/goldilocks.png)

---
# Goldilocks

## On peut faire "moins moche..."

- Parlons Kube-State-Metrics, mieux : CustomResourceStateMetrics !!

```yaml
# kubectl get vpa goldilocks-example -o yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
[...]
status:
  recommendation:
    containerRecommendations:
    - target:
        cpu: 15m
        memory: "1238659775"
```

<!--
_footer: https://www.vrchr.fr/posts/2024/2024-01-09-kube-state-metrics-vpa/
-->

---
<!--
_transition: slide
-->

# Goldilocks + VPA + Grafana = ❤️

![center width:1000px](assets/grafana_vpa.png)

---
<!--
_transition: slide
-->

# Mes stratégies

## Mes Outils
- Goldilocks + VPA
- Prometheus + Grafana + CustomResourceStateMetrics

##  Requests, Limits ?
- CPU Requests, No Limits
- RAM Requests = Limits
- ➡️ Ce dont l'application à **besoin** ★

<!--
- Requests trop basses & HPA
-->

![bg right:40% fit](assets/requests-limits.png)

<!--
_footer: https://home.robusta.dev/blog/stop-using-cpu-limits
-->

---
# Nouveautés depuis K8S 1.27

- **In-place Resource Resize for Kubernetes Pods (alpha)**
  "*In Kubernetes v1.27, we have added a new alpha feature that allows users to resize CPU/memory resources allocated to pods without restarting the containers*"
  ➡️ 📺 [To Infinity and Beyond: Seamless Autoscaling with in-Place Resource Resize for Kubernetes Pods](https://sched.co/1YePO) (KubeCon EU 2024)

![bg right:30% 90%](assets/vpa-inplace.png)

---
<!--
_transition: slide
-->
# Ressources

## Blog posts

- [Kubernetes CPU Limits (cgroup quotas)](https://sysdig.com/blog/kubernetes-cpu-requests-limits-autoscaling/#kubernetes-cpu-limits)
- [Stop setting CPU & Memory Requests](https://thenewstack.io/stop-setting-cpu-and-memory-requests-in-kubernetes/)
- [Stop Using CPU limits](https://home.robusta.dev/blog/stop-using-cpu-limits)
- [The Case for Kubernetes Resource Limits: Predictability vs. Efficiency](https://kubernetes.io/blog/2023/11/16/the-case-for-kubernetes-resource-limits/)
- [Why does my 2vCPU application run faster in a VM than in a container?](https://hwchiu.medium.com/why-does-my-2vcpu-application-run-faster-in-a-vm-than-in-a-container-6438ffaba245)
- 🤔 🤯 [Resize CPU Limit To Speed Up Java Startup on Kubernetes](https://piotrminkowski.com/2023/08/22/resize-cpu-limit-to-speed-up-java-startup-on-kubernetes/)

---
<!--
_class: last
-->

# Merci

## Rémi Verchère <br/> Accenture
