# Make CI/CD Simpler

to-be-continuous : Template de pipeline complets
r2devops :
  Un bout des pipelines, facilité / marketplace
  Flexible & accessible
  Espace innersource

Pipeline complet accessibles à tout le monde

Partage : t'es plus tout seul


## Intro

1er pipeline, cool
2eme, copier/coller
3eme: templates
4eme: maj du template et OUPS pas de versionning
5eme: ....

## To Be Continuous

- FR: https://docs.google.com/presentation/d/1Kss9myj9QEhb9GtmY1AIXuDFgSO39klVGFpqLuNnlhI/edit?usp=share_link
- EN: https://docs.google.com/presentation/d/1mMX7CbzhnIhUAa6RRznmzaCEhI3WBY01d8-NW-7hAmA/edit?usp=share_link
- Vidéo YT: https://www.youtube.com/watch?v=LOWnPgWXXjU
- Samples : https://gitlab.com/to-be-continuous/samples


https://to-be-continuous.gitlab.io/doc/


### Pourquoi

Besoin de standardisation

Outils de CI/CD proposent pleins de choses, mais pas de bonnes recommendations ? Pas d'opinion (flux, workflow, etc..). Cf Git avec les git flows.

Catalogue de jobs de CI/CD modulaires (templates)
- prêt à l'emploi, ça fonctionne par défaut
- modulaires
- composables
- configurables
- surchargeables

Avril 2019, (inner source Orange), juste après que la notion de templates sous GitLab soit dispo
Juin 2021, open source

### Comment ça marche

Include de templates
Versionning (semantic versionning)

33 Templates
- Build (langage)
- Analyse de code
- Packaging
- Infra & Cloud
- Acceptance
- Autre (semantic)

### Démarrer

La Doc

Kicker : configurateur en ligne

Projets d'exemples

Voir https://docs.gitlab.com/ee/ci/review_apps/

Exemple: https://gitlab.com/tbc-playground/python-demo

Pipelines définis lors de merge requests

### Détails / Principes

- Standardisation du séquencement des tâches (squelette) : stages
- Modularité / Composabilité : output -> input d'un stage à l'autre
- DevSecOps : tous les templates proposent de l'analyse de code
- Workflow GIT : Environnement de deploiements. Préconisé : feature branch (efficace)
- Cycle de dev optimisé : rapidité & qualité
  - le dev s'occupe du fonctionnel, du coup pas de lint / etc, mais exécutable à la demande
  - lors de la MR: obligration de lint & co
  - sur branche d'intégration / prod : pipeline qui fail
- Proxies d'entreprises / CA : variables d'environnements

To Be Continuous fait des choix pour vous

Si besoin sur du sel-hosted : synchro du dépot SAAS

## R2DevOps

### Talk R2DevOps

Janvier 2020 : création de la société pour simplifier la vie des devs

Sondage GitLab / Github / Autre
Sondage CI/CD

Pb principal : étapes à la main par les devs, alors que c'est pas leur job (dépendances, test U, build docker, ....)
- chronophage
- environnement spécifique
- compétences dev ET ops
- erreur humaine
 -> CI/CD: tout ce que les devs veulent pas faire à la main

 En plus : Developer Experience, éviter de se poser trop de questions pour coder

 grid-api: flask

 1ere question du dev : qu'est-ce qu'on automatise ? (Toil)

- Quels stages ?
- Quel env (image docker) ?
- Quels artefacts ?

GitLab templates: https://gitlab.com/gitlab-org/gitlab-foss/-/blob/master/doc/development/cicd/templates.md
https://docs.gitlab.com/ee/development/cicd/templates.html
https://docs.gitlab.com/ee/ci/examples/index.html#cicd-templates



- Tests des pipelines

- Problème de maintenance
- SPOF : car quand pipeline HS, dev paralysé
- Supply Chain attack (récupération de ressources externes) !

R2DevOps : source de confiance / marketplace des jobs

Override des variables / stages / etc

Partie innersource / privée du cataloqgue

Duplication de la CI/CD


Anchors & co

Job :
- simple & auto-suffisant
- documenté
- versionning

2eme étape: éditeur de pipeline web, a peu près équivalent gitlab editor, possibilité no-code, état du pipeline, recherche, etc...

3eme étape: plateforme agnostique, à voir pour fonctionner sur GitHub Actions & co --> Dagger ?

Version SAAS et Self Hosted

"Bonnes pratiques"
