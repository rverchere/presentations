---
marp: true
theme: gaia
markdown.marp.enableHtml: true
---
<style>
section {
  background-color: #fff;
  background-image: none;
  color: #3f3f3f; 
  font-family: 'arial';
  font-size: 25px;
}
section.lead {
  background-color: #fff;
  font-size: 32px;
}
section.lead h1 {
  color: #1d428a;
  font-size: 48px;
}
section.lead h2 {
  color: #1d428a;
  font-size: 36px;
}
section.lead a {
  color: #1d428a;
}
h1 {
  color: #1d428a;
}
h1 strong {
  color: #3f3f3f;
}
h2 {
  color: #1d428a;
}
h3 {
  color: #1d428a;
}
a {
  color: #1d428a;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
table {
    width: 100%;
}
</style>

<!--
_class: lead
_transition: flip
-->

![bg left:25% fit 75%](assets/vmugfr-logo-blason-web.png)

# Vos pipelines de CI/CD efficaces
# et sans prise de tête*

## * *Enfin, pas trop...*

*Rémi Verchère @ Accenture*

![width:200px](assets/vmug_logo.png)


---
![bg cover](assets/vmug_background_2.png)

&nbsp;

## Au sommaire
- Rappel de quelques concepts CI/CD & GitLab-CI
- Problématiques de gestion à l'échelle
- Présentation d'outils pour nous aider + démos associées
- *Mes* bonnes pratiques *(ou mauvaises, vous jugerez par vous-même)*

## La démo
- Application Python Flask
- Création d'image Docker
- Déploiement sur Kubernetes
- https://gitlab.com/opsrel.io/k8s-app-flask

---
<!--
_transition: flip
-->

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

- Ops qui aime bien les Devs

- Papa x 👦👦👧👧

- GitLab Certified Associate

- CIVO & R2Devops Ambassador

- 🌐 *@rverchere*

- 📖 https://presentations.verchere.fr/

![bg fit right:40%](assets/photo-profil.png)
![bg fit right:40% 70%](assets/Acc_GT_Dimensional_RGB.svg)


---
# Instant pub

![bg fit 65%](assets/vmugfr-logo-blason-web.png)
![bg fit 85%](assets/civo-logo.png)
![bg fit 100%](assets/logo-r2.png)
![bg fit 75%](assets/Acc_GT_Dimensional_RGB.svg)

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# CI / CD

---
# CI / CD

> *Wikipedia*: En génie logiciel, CI/CD (parfois écrit CICD) est la combinaison des pratiques d'**intégration continue** et de **livraison continue** ou de **déploiement continu**.
>
> *Le CI/CD comble le fossé entre les activités et les équipes de développement et d'exploitation en imposant l'automatisation de la création, des tests et du déploiement des applications. Les pratiques DevOps modernes impliquent le développement continu, le test continu, l'intégration continue, le déploiement continu et la surveillance continue des applications logicielles tout au long de leur cycle de vie. La pratique CI/CD, ou pipeline CI/CD, constitue l'épine dorsale des opérations DevOps modernes.*

---
![bg 80%](assets/gitlab_workflow_example_extended_v12_3.png)

---
# GitLab-CI

- Solution de CI/CD proposée par GitLab
- Fichier `.gitlab-ci.yml` qui définit les actions de CI/CD
- Runners qui les exécutent et renvoient le résultat au serveur GitLab
- *Cette présentation a été générée via un pipeline de CI/CD*

![center width:580px](assets/gitlab-runner-archi.png)

---
# GitLab-CI

## Un peu de vocabulaire
- Pipelines
- Stages
- Jobs

![center width:1100px](assets/example-pipeline-1.png)

---
# CI / CD

## Exemple de job

```yaml
python_lint:
  image: "python:3-slim"
  stage: test
  before_script:
    - pip install -r app/requirements.txt
  script:
    - pylint -d C0114,C0116 app/*.py
  only:
    refs:
      - main

```

- Mots-clés : voir https://docs.gitlab.com/ee/ci/yaml/

---
# CI / CD

## Mise en oeuvre

- Quels stages ?

- Quels jobs, et on met quoi exactement dans les jobs ?

- Quel environnement d'exécution pour les jobs ?

- Workflow Git, mais pas de workflow CI/CD ?

![center width:400px](assets/03_Release_branches.svg)

---
<!--
_transition: flip
-->

![bg fit right:40%](assets/twitter-ci.png)
# CI / CD

## Mise à l'échelle

- Comment partager (`templates`),
  réutiliser (`include`) ?

- Comment rechercher ?

- Comment versionner ?

- Comment rester "simple" (`anchors`, ...) ?
  https://gitlab.com/CalcProgrammer1/OpenRGB/-/blob/master/.gitlab-ci.yml

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# R2Devops

---
# R2Devops

- **Marketplace** de **jobs** de CI/CD, pour améliorer la vie des devs (*qui ont autre chose à faire que d'écrire des pipelines*), **éditeur** intégré, **dashboard**

- Créé en 2020 par la société GO2scale @ Montpellier.

- **Catalogue** de jobs officiels, communautaires, partenaires, public ou privé.

- **Informations** sur l'utilisation, conformité, sécurité, etc. de la supply chain

- **Plateforme** Saas ou On Premise

- Pour aller plus loin
  - Site Web : https://r2devops.io/
  - Serveur Discord : https://discord.gg/FmxtX8CWBH
  - Comment concilier CI/CD et Developer Experience ? https://youtu.be/tOtAFoDY6BM

---
<!--
_transition: flip
-->
<style scoped>
h1 {
  color: #fff;
}
</style>

![bg fit 98%](assets/vmug_background_4.png)

# R2Devops

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# To Be Continuous

---
# To Be Continuous

- **Templates** de **pipelines complets**, accessibles à tout le monde

- Projet initié chez Orange en 2019, "Open sourcé" en 2021

- **Standardisation** complète, *opiniated* pipelines, workflows, stages.

- Configurateur en ligne **kicker**

- Pour aller plus loin
  - Site Web : https://to-be-continuous.gitlab.io/
  - Serveur Discord : https://discord.gg/c9qnAXRssn
  - « To Be Continuous », mon pipeline GitLab CI en quelques minutes:  https://youtu.be/LOWnPgWXXjU

---
<!--
_transition: flip
-->
<style scoped>
h1 {
  color: #fff;
}
</style>

![bg fit 98%](assets/vmug_background_5.png)

# To Be Continuous

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# One more thing™

---
# R2devops + To Be Continuous ??

- R2devops : Developer Experience, Marketplace, Facilité de partage
- To Be Continuous : Pipelines complets, Workflows, Environnements de déploiement

# Collaboration !

- 🫶 R2Devops and To Be Continuous : collaborating to improve Open-Source CI/CD (24 avril 2023)!
https://blog.r2devops.io/blog/All/r2devops_and_to_be_continuous/

---
<!--
_transition: flip
-->
<style scoped>
h1 {
  color: #fff;
}
</style>

![bg fit 98%](assets/vmug_background_4.png)

# R2devops + To Be Continuous

&nbsp;

![center](assets/showtime.gif)

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# Prenons de la hauteur

---
<!--
_transition: flip
-->

# Dashboard R2Devops

- Maintenabilité de la CI/CD
  - Utilisation
  - Conformité
  - Mise à jour
- Sécurité de la Supply Chain
  - Leak de données, variables
  - Vulnérabilités des images
 
![bg fit 80% right:50%](assets/r2devops-dashboard.png)

---
<!--
_class: lead
-->

![bg cover left:40%](assets/vmug_background_3.png)

# ***Mes*** bonnes pratiques

---
# Mes bonnes pratiques

- Keep It Simple Stupid
  - 1 job auto-suffisant
  - Pas forcément besoin de gérer TOUS les cas d'usage si maitrisé (cf rôles Ansible)
  - Pas d'over-engineering (cf [Generated & Parent-Child Pipelines](https://www.vrchr.fr/posts/2022/2022-04-04-helm-chart-releaser-gitlab/))

![center width:800px](https://www.vrchr.fr/2022/04/2022-04-04-gitlab-pipelines.png)

---
# Mes bonnes pratiques

- 1 projet de templates de jobs, ouvert à la collaboration.

- Proposer des pipelines par défaut, utilisant les templates de jobs.

- Documentation :
  - Présentation générale des templates
  - Par Job
  - Par Pipeline
  - Nomenclature simple
  - Changelogs pour aider les devs


![bg right:48% 90%](assets/gitlab-templates-doc.png)

---
<!--
_transition: flip
-->

# Mes bonnes pratiques

- Points de détails:
  - ⚠️ Variables globales vs locales: hiérachie
  ```yaml
  include:
    - remote: path/to/my_template'
      ref: tag-1.0.0
  variables:
    MY_GLOBAL_VAR: "VMUG"

  my_job:
    variables:
      MY_LOCAL_VAR: "VMUG FR"
  ```

  - ⛔ Versionnez vos jobs (+ changelog cf slide précédente)

- 😇 Inspirez-vous de R2Devops & To Be Continuous

- 😁 Même mieux, utilisez-les !

- 🤯 Encore mieux, contribuez !!

---
<!--
_class: lead
-->

![bg cover](assets/vmug_background_2.png)

# Vos questions & merci !

## *Rémi Verchère @ Accenture*

*J'ai des stickers pour vous ;)*

![width:200px](assets/vmug_logo.png)
