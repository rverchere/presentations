---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<style>
section {
  background-color: #fff;
  background-image: none;
  color: #000; 
  font-family: 'Source Sans Pro';
  font-size: 28px;
}
section.lead {
  background-color: #00074C;
  background-image: url("assets/vtt-intro.png");
  background-size: cover;
  color: #fff;
}

section.lead h1 {
  color: #fff;
}
section.lead h2 {
  color: #fff;
}
section.lead a {
  color: #ccc;
}
h1 {
  color: #00074C;
}
h1 strong{
  color: #00074C;
}
h2 {
  color: #8083A4;
}
h2 strong{
  color: #8083A4;
}
h3 {
  color: #aaa;
}
h3 strong{
  color: rgb(104, 45, 145);
}
a {
  color: #609BCC;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
table {
    width: 100%;
}
</style>

<!--
_class: lead
-->

# &nbsp;
# Velero: backup and restore (well) your Kubernetes applications

*Rémi Verchère @ Accenture*

---
## Foreword

- I'm not a backup expert, doing this only by 🎷 altruism 👐
- No live coding, only REX  🦖

## Agenda
- Tool introduction
- Production usage, data management, etc.
- Data migration example
- Some fails, as you love it ❤️

---
<!--
_transition: flip
-->

# Who am I?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

- Ops who likes Devs

- Dad x 👦👦👧👧

- Certified Kubernetes ☸️ {Associate,Dev,Admin,Security}

- 🌐 *@rverchere*

- 📖 https://presentations.verchere.fr/

![bg fit right:40%](assets/photo-profil.png)
![bg fit right:40% 70%](assets/Acc_GT_Dimensional_RGB.svg)

---
<!--
_class: lead
-->

# Backup, Kubernetes?

---
# Backup & Kubernetes

## Why?

- Stateless containers
- YAML Manifest easily re-deployable
- CI/CD
- GitOps
- Reconciliation loop
- `kubectl apply || crictl run` and done ^^
- Many other approximative reasons...

---
# Backup & Kubernetes

## Because, sometimes, in real life...

- Statefull containers
- Migration & Data protection
- Disaster recovery
- Resources in prod not "exactly" the same <!-- Mutation Wehbook -->
- Human error 👾

```bash
$ kubectl delete namespace la-prod
```

---
# Backup & Kubernetes

![bg 90%](assets/velero.png)

<!--
Notes: Je hais les backups, mais je hais encore plus perdre des données.
Depuis quelques temps j'administre des cluster k8s & applications qui tournent dessus.
On m'a présenté Velero comme l'outil magique,  mais j'y crois pas trop ;)
J'ai pas trouvé bcp de ressources FR sur le sujet, d'où ce talk pour partager avec vous mes galères !
-->

---
# Velero - General information

## Some history
- Heptio Ark, "open-sourced" mid 2017
- Company bought by VMware end of 2018
- Integrated in Tanzu offer (TMC)
- Today in version 1.11.0 (2023-04-20)

![bg 90% right:33%](assets/heptio.jpg)

```
$ git log --reverse
commit 2fe501f527a88ea292ca3dde80992ec60b388dda
Author: Andy Goldstein <andy.goldstein@gmail.com>
Date:   Wed Aug 2 13:27:17 2017 -0400
```

---
<!--
_footer: https://velero.io/docs/main/index.html
-->
# Velero - General information

## Common features

- **Backup** cluster resources and **restore** it in case of loss.
- **Migration**, **Replication** of resources from a cluster to another.
- **Custom Resources** defining what to backup, where and when (selector, schedule).
<!--
Pas de sauvegarde d'ETCD / Control planes, on reste sur l'applicatif
-->

## Components

- Server (Controller) running in the cluster.
- Command line client running locally on your machine.

---
<!--
_footer: https://docs.ondat.io/docs/usecases/velero-backups/
-->
# Velero - Architecture

![bg 90%](assets/velero-backup-process.jpg)

<!--
1. The Velero client makes a call to the Kubernetes API server to create a Backup object.
2. The BackupController notices the new Backup object and performs validation.
3. The BackupController begins the backup process. It collects the data to back up by querying the API server for resources.
4. The BackupController makes a call to the object storage service – for example, AWS S3 – to upload the backup file.

-->
---
# Velero - Installation

## Cluster: Helm chart

```bash
$ helm repo add vmware-tanzu https://vmware-tanzu.github.io/helm-charts && helm repo update
$ helm install velero vmware-tanzu/velero --create-namespace -n velero
Release "velero" has been installed. Happy Helming!
```

## Client: CLI

```bash
$ asdf plugin add velero && asdf install velero 1.11.0 && asdf global velero 1.11.0
$ velero version
Client:
        Version: v1.11.0
        Git commit: 0da2baa908c88ec3c45da15001f6a4b0bda64ae2
Server:
        Version: v1.11.0
```

---
<!--
_footer: Aurélie Vache - Understanding Kubernetes in a visual way
-->

![bg width:800px](assets/understanding_velero_in_a_visual_way.png)

---
<!--
_footer: Aurélie Vache - Understanding Kubernetes in a visual way
_transition: flip
-->

![bg width:800px](assets/understanding_velero_in_a_visual_way_2.png)

---
<!--
_class: lead
-->

# Scenario: "The" production

---
# Once upon a time the "prod de confiance"

## Context 🇫🇷

- Many Kubernetes managed clusters **OVHcloud**
  - Object storage: Openstack **Swift** or Standard **S3**
  - Block storage: **Cinder**
- **Heterogeneous** applications
- Infrastructure management tools mostly **Open Source**
- Backup needs:
  - **S3** storage, **far away**
  - Applications **replicated** on a test cluster
  - DRP on **other** cloud provider (agnosticity)

---
# Application example

![ bg right:25% fit](assets/Nextcloud.png)

## Nextcloud

- Helm Chart
- 1 application deployment
- 1 Mariadb database
- 1 Redis cache (master / replicas)
- Ingress, Cronjobs, etc.

https://github.com/nextcloud/helm

---
<!--
_transition: flip
-->

# Application example

## Nextcloud

```
$ kubectl get deployment,statefulset,cronjob,service,ingress
NAME                        READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/nextcloud   1/1     1            1           28d

NAME                                        READY   AGE
statefulset.apps/nextcloud-mariadb          1/1     28d
statefulset.apps/nextcloud-redis-master     1/1     28d
statefulset.apps/nextcloud-redis-replicas   1/1     28d

NAME                           SCHEDULE       SUSPEND   ACTIVE   LAST SCHEDULE   AGE
cronjob.batch/nextcloud-cron   */15 * * * *   False     0        62s             28d

NAME                               TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
service/nextcloud                  ClusterIP   10.3.124.75    <none>        8080/TCP   28d
service/nextcloud-mariadb          ClusterIP   10.3.145.33    <none>        3306/TCP   28d
service/nextcloud-redis-headless   ClusterIP   None           <none>        6379/TCP   28d
service/nextcloud-redis-master     ClusterIP   10.3.207.110   <none>        6379/TCP   28d
service/nextcloud-redis-replicas   ClusterIP   10.3.249.214   <none>        6379/TCP   28d

NAME                                  CLASS    HOSTS                     ADDRESS                            PORTS     AGE
ingress.networking.k8s.io/nextcloud   <none>   nextcloud.demo.vrchr.fr   ip-135-125-84-204.gra.lb.ovh.net   80, 443   28d
```

---
<!--
_class: lead
-->

# Backup & Restore, the aventure...

---
# Velero - First time

## 1st backup

```
$ velero backup create nextcloud-backup-1 --include-namespaces nextcloud

$ velero backup describe nextcloud-backup-1
Name:         nextcloud-backup-1
Namespace:    velero
[...]
Phase:  Completed

Errors:    0
Warnings:  0
[...]
Total items to be backed up:  200
Items backed up:              200
[...]
```

---
# Velero - First time

## 1st restore

```
$ velero restore create nextcloud-restore-1 --from-backup nextcloud-backup-1

$ velero restore nextcloud-restore-1
Name:         nextcloud-restore-1
Namespace:    velero
Labels:       <none>
Annotations:  <none>

Phase:                       Completed
Total items to be restored:  200
Items restored:              200
```

---
<!--
_transition: flip
-->

# Velero - First time

## 1st fail

> Kubernetes objects saved, data sacrificed!

![bg right:38% 95%](assets/backup_pvc.jpg)

![center width:420px](assets/nextcloud_welcome.png)

---
<!--
_class: lead
-->

# Velero Snapshots, Backups, Restore
# Persistent Volumes 

---
# Velero - Data backup

## Kubernetes Objects 

Kubernetes objects are exported to a S3 bucket

## Persistent data: 2x2 possibilities

1. **Snapshots** of Persistent Volumes (PV)
   - via Cloud Provider **API** 
   - via **CSI** (*>= version 1.4*). ⚠️  Cloud Provider compatibility (Cinder: ✅)
<!--
CSI: Container Storage Interface
-->

3. **Export** of mounted volumes from pods: File System Backup **FSB** (Swift and S3: ✅)
    - via **Restic**
    - via **Kopia** (*>= version 1.10*)

---
# Velero - Snapshot PV via CSI

![ center ](assets/velero_backup_csi.jpg)

---
# Velero - Snapshot PV via CSI

## Activation with Helm chart

If you CSI supports it: `--features=EnableCSI`
```yaml
# $ helm get values velero
snapshotsEnabled: true
[...]
configuration:
  [...]
  features: EnableCSI
[...]
initContainers:
  - name: velero-plugin-for-csi
    image: velero/velero-plugin-for-csi:v0.5.0
    volumeMounts:
      - mountPath: /target
        name: plugins
```

---
# Velero - Snapshot PV via CSI

```shell
$ velero backup get                                       
NAME                            STATUS      ERRORS   WARNINGS   CREATED                          EXPIRES   STORAGE LOCATION   SELECTOR
daily-s3-20221004182135         Completed   0        0          2022-10-04 20:21:35 +0200 CEST   1d        velero-gra9        <none>
daily-s3-20221004181435         Completed   0        0          2022-10-04 20:14:35 +0200 CEST   1d        velero-gra9        <none>
[...]
daily-snapshot-20221004190535   Completed   0        0          2022-10-04 21:05:35 +0200 CEST   1d        velero-gra9        <none>
daily-snapshot-20221004190035   Completed   0        0          2022-10-04 21:00:35 +0200 CEST   1d        velero-gra9        <none>
daily-snapshot-20221004185535   Completed   0        0          2022-10-04 20:55:35 +0200 CEST   1d        velero-gra9        <none>
daily-snapshot-20221004185035   Completed   0        0          2022-10-04 20:50:35 +0200 CEST   1d        velero-gra9        <none>
[...]
```

⚠️ Limits with storage provider

⚠️ Not considered as "persistent" storage

---
# Velero - Backup via File System Backup (FSB) 

![ center ](assets/velero_backup_restic.jpg)

---
# Velero - Backup via FSB (Restic)

## Activation with Helm chart

```yaml
# $ helm get values velero
deployNodeAgent: true
# Daemonsets deployment on nodes
[...]
configuration:
  [...]
  uploaderType: restic # By default for now
  defaultVolumesToFsBackup: true
[...]
initContainers:
  # Already defined for k8s objects export
  - name: velero-plugin-for-aws
    image: velero/velero-plugin-for-aws:v1.7.0
    volumeMounts:
      - mountPath: /target
        name: plugins
```

---
# Velero - Backup via FSB (Restic)

## 2 backups **modes**

1. **Opt-in**: nothing by default, we select what we need to backup.
```yaml
podAnnotations:
  ## Velero annotations
  backup.velero.io/backup-volumes-includes: nextcloud-main
```

2. **Opt-out**: everything by default, we select what we want to exclude from backup.
   *By default from Velero 1.5*
```yaml
podAnnotations:
  ## Velero annotations
  backup.velero.io/backup-volumes-excludes: redis-data
```

---
<!--
_transition: flip
-->

# Velero - Restore

- Restore process "**almost**" the opposite of a backup, a bit more:

  - **Validation** of resources compatibility 

  - **Order** of resources restore

  - Ability to change the destination **namespace** `--namespace-mappings`

  - Ability to change the **storage class**`velero.io/change-storage-class`

  - "***Incremental***" mode `--existing-resource-policy`

## Data (PV/PVC)

- If Snapshot: **Restore** of the volume, then **remapping** / reference

- If FSB: **Creation** of a new volume, data **rehydration**

---
<!--
_class: lead
-->
# Snapshots or FSB?

---
# Velero - Snapshots ou FSB ?

| Snapshot | FSB      |
|---------|--------------|
| Non atomic | Non atomic |
| Same media | Different media |
| Faster | Slower |
| Near | Far (⚠️ transfert costs) |
| Unencrypted(⚠️ Storage Class)| Encrypted |
| Same Cloud Provider (⚠️ Zone) | Cloud Provider Agnostic |

---
<!--
_footer: https://www.msp360.com/resources/blog/following-3-2-1-backup-strategy/
-->
# Velero - Snapshots or FSB?

## Good practice reminder

![center width:800px](assets/3-2-1-Backup-Rule.png)

*Even 3-2-1-1*

---
# Velero - Snapshots or FSB?

## **Both**! But...

- Well configure **opt-in** / **opt-out** for FSB (**defaultVolumesToFsBackup**)

- Well configure **includes** / **excludes** for volumes

- Well configure snapshot usage (`--snapshot-volumes`)

⚠️ Some combinations don't work together

- If **opt-in** && **includes**: no volume snapshot

```shell
time="2022-10-02T00:01:04Z" level=info msg="Skipping snapshot of persistent volume \
because volume is being backed up with restic."
```

---
<!--
_transition: flip
-->

# Velero - Snapshots or FSB?

## My recommendations

- **opt-out** by default

- 1 schedule classic: backup S3, with **excludes** for some volumes, without snapshots (**snapshotVolumes: false**)

- 1 schedule snapshots: **defaultVolumesToFsBackup: false** (***opt-in***)

- If some doubt: **FSB**
   - Restic vs Kopia: https://velero.io/docs/v1.10/performance-guidance/

---
<!--
_class: lead
-->
# 1 point about hooks...

---
<!--
_transition: flip
-->

# Velero - Hooks

- **Pre** & **Post** Hooks

- **Pod Annotation** or **Backup Spec**

  - `pre.hook.backup.velero.io/container`

  - `pre.hook.backup.velero.io/command`

  - `pre.hook.backup.velero.io/on-error`

  - `pre.hook.backup.velero.io/timeout`


⚠️ command must be available, otherwise use sidecar container

<!-- ⚠️ `fsfreeze` & montages NFS -->

---
<!--
_class: lead
-->
# And 1 other on Databases!

---
<!--
_transition: flip
-->

# Velero - DB & Atomicity

1. Using **hooks** with `fsfreeze`, `sqldump`
   ⚠️ DB lock during backup time

2. Using DB read-only replica
   Backup hook on **replica**, *oldschool way*

![bg right:33% fit](assets/kanister_blueprints.png)

3. Use Kubernetes Operators

4. **Outside** Kubernetes cluster

5. Usage of **Managed DB** 🙆

- Backups blueprints? See **Kanister**


---
<!--
_class: lead
-->

# 🌩️ Cloud migration use case 🌩️

---
# Velero - Cloud migration use case
![ center width:1200px](assets/velero_backup_restic_migrate_02.jpg)

<!--
# Velero - Cas d'une migration cloud

- Installation de Velero sur les 2 clusters.

  - Configuration des mêmes Storage Location (Read Only)

- Backup Velero Cluster A, sur Bucket S3 avec Restic

- Restore sur Cluster B, depuis même Bucket S3 avec Restic

- Facile ! Ou pas...
-->

---
# Velero - Cloud migration use case

- Pay particular attention to:

  - DNS changes (CQFD)

  - Kubernetes & API versions: "API Group Versions Feature"
  
  - Some unnecessary resources: "--exclude-resources"
  ```shell
  $ velero restore --exclude-resources CustomResourceDefinition,CertificateRequest,Order
  ```

  - Different storage: "Changing PV/PVC Storage Class"
  ```yaml
  kind: ConfigMap
  metadata:
    labels:
      velero.io/change-storage-class: RestoreItemAction
  data:
    <old-storage-class>: <new-storage-class>
  ```


---
# Velero - Cloud migration use case

  - Restore time can be **long** (if hundreds of GB)
    How can we optimize this migration time?
    - Flag `--existing-resource-policy` for incremental restore?

![ center width:1000px](assets/velero_backup_restic_migrate_03.jpg)

---
# Velero - Cloud migration use case

  - Restore time can be **long** (if hundreds of GB)
    How can we optimize this migration time?
    - Answer: **rsync**!

![ center width:1000px](assets/velero_backup_restic_migrate_04.jpg)

---
# OpenShift MTC

![center width:800px](assets/openshift_migration_toolkit.png)

---
<!--
_transition: flip
-->

# OpenShift MTC

> *The Migration Toolkit for Containers (MTC) enables you to migrate stateful application workloads between OpenShift Container Platform 4 clusters at the granularity of a namespace.*

> *The file system copy method uses **Restic** for indirect migration or **Rsync** for direct volume migration.*

*https://docs.openshift.com/container-platform/4.11/migration_toolkit_for_containers/about-mtc.html*

---
<!--
_class: lead
-->

# ✨ Some fails ✨

---
# Velero - Failed backup

1. **Storage Location**
   Forgot to specify, Velero does not know where to back up
```yaml
spec:
  storageLocation: ovh-velero-storage-location
```

```
$ kubectl logs -f velero-7dcfbb4b6b-k8fl9
time="2023-01-31T03:09:07Z" level=warning msg="There is no existing backup storage location set as default.
Please see `velero backup-location-h` for options." controller=backup-storage-location
logSource="pkg/controller/backup_storage_location_controller.go:173"
```

2. **Timeout**
   On big volumes, Restic timeout
```yaml
configuration:
  resticTimeout: 2h
```

---
# Velero - Failed backup

3. **S3 Objects**
   Backup too big (a lot of namespaces)

![center width:800px](assets/bucket_s3.png)

---
# Velero - Failed backup

4. **Restic OOMKilled!**
   Increase requests & limits (big update since Restic 0.14.0)
   GOGC ! (https://tip.golang.org/doc/gc-guide)
   ⚠️  Ressources Daemonset
```yaml
configuration:
  extraEnvVars:
    GOGC: 10
resources: # Velero
  limits:
    cpu: null
    memory: 2Gi # <--- ⚠️ Restic Prune !
restic:
  resources:
    limits:
      cpu: "2"
      memory: 4Gi
```

---
# Velero - Failed backup

5. **Restic Locked backend**
   Multi clusters: be carefull on storage location RW

```log
stderr=unable to create lock in backend: repository is already locked exclusively by PID 11108
on velero-76cfbd7858-5fr8t by nonroot (UID 65532, GID 65532)
lock was created at 2022-09-30 04:00:05 (1m5.593684965s ago)
storage ID 44a72b6f the `unlock` command can be used to remove stale locks
exit status 1" error.file="/go/src/github.com/vmware-tanzu/velero/pkg/restic/backupper.go:184"
error.function="github.com/vmware-tanzu/velero/pkg/restic.(*backupper).BackupPodVolumes"
logSource="pkg/backup/backup.go:417"
name=nextcloud-mariadb-0
```   

```
$ kubectl patch backupstoragelocation <STORAGE LOCATION NAME> \
    --namespace velero \
    --type merge \
    --patch '{"spec":{"accessMode":"ReadOnly"}}'
```

---
# Velero - Failed restore

1. **Restic Annotations**
   Forgot to annotate 1 volume 🤭

```yaml
podAnnotations:
  backup.velero.io/backup-volumes-includes:
```

2. **Selector**
   Not every resource has been selected
```
$ kubectl get all -l app=mariadb-database
NAME                      READY   STATUS    RESTARTS   AGE
pod/mariadb-database-0   1/1     Running   0          93d
```


---
# Velero - Failed restore

3. **Corrupted** DB!
   No atomicity:  using pre-hooks & post-hooks
   on Backup, but **also** on Restore
```yaml
metadata:
      annotations:
        backup.velero.io/backup-volumes: data
        pre.hook.backup.velero.io/command: '["/bin/bash", "-c",
          "mkdir -p /bitnami/mariadb/backups \
          && mysqldump -u $MARIADB_USER -p$MARIADB_PASSWORD \
          $MARIADB_DATABASE > /bitnami/mariadb/backups/nextcloud.dump"]'
```

One shall also retrieve dumps (tar)...
```shell
$ kubectl cp nextcloud-mariadb-0:~/nextcloud.dump ./nextcloud.dump                                                         
command terminated with exit code 126
```

---
<!--
_transition: flip
-->

# Velero - Failed restore

4. Statefulset missing?!
   Bug with *velero.io/change-storage-class*

```shell
$ velero restore create nextcloud-migration-22091501 --from-backup nextcloud-22091501
```

```shell
$ velero restore describe nextcloud-migration-22091501
[...]
Phase: PartiallyFailed (run 'velero restore logs nextcloud-migration-22091501' for more information)
Total items to be restored:  105
Items restored:              105

Started:    2022-09-15 21:02:46 +0200 CEST
Completed:  2022-09-15 22:13:04 +0200 CEST

[...]

Errors:
  Namespaces:
    nextcloud:  error preparing statefulsets.apps/nextcloud/nextcloud-mariadb: rpc error: code = Aborted desc = plugin panicked: \
                runtime error: invalid memory address or nil pointer dereference
                error preparing statefulsets.apps/nextcloud/nextcloud-redis-master: rpc error: code = Aborted desc = plugin panicked: \
                runtime error: invalid memory address or nil pointer dereference
[...]
```

---
<!--
_class: lead
-->

# Velero, even more!

---
# Monitoring

```yaml
metrics:
  serviceMonitor:
    enabled: true
```

```
# HELP velero_backup_attempt_total Total number of attempted backups
# HELP velero_backup_deletion_success_total Total number of successful backup deletions
# HELP velero_backup_duration_seconds Time taken to complete backup, in seconds
# HELP velero_backup_failure_total Total number of failed backups
# HELP velero_backup_items_errors Total number of errors encountered during backup
# HELP velero_backup_validation_failure_total Total number of validation failed backups
# HELP velero_csi_snapshot_attempt_total Total number of CSI attempted volume snapshots
successful volume snapshots
# HELP velero_restore_attempt_total Total number of attempted restores
# HELP velero_restore_total Current number of existent restores
# HELP velero_restore_validation_failed_total Total number of failed restores failing validations
# HELP velero_volume_snapshot_attempt_total Total number of attempted volume snapshots
```

---
# Monitoring

![center width:1000px](assets/velero_monitoring.png)

---
# Alerting !

![ center width:900px](assets/velero_alert.png)

---
# Backup Policies

## Kyverno

![center](assets/kyverno.png)

> https://kyverno.io/policies/?policytypes=Velero

---
<!--
_transition: flip
-->

## Last advices

- Read Velero **doc**!
- **Verify** backups
- Validate restores **regularly**
- You don't have to put **everything** in Kubernetes, see **managed services** 🙃
- **For real, Velero works really fine, but you have to configure it well.**

## They are using Velero

- Openshift "OADP" & "MTC"
- VMware "TMC"
- Accenture 😬
- You? Let's have a talk!

---
<!--
_class: lead
-->

# Thank you!

*Rémi Verchère @ Accenture*
