---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<style>
section {
  background-color: #fff;
  background-image: none;
  color: #000; 
  font-family: poppins, sans-serif;
  font-size: 28px;
}
section.lead {
  background-color: #302A43;
  background-image: url("assets/cirque-du-lys.jpg");
  background-size: cover;
  color: #302A43;
}

section.lead h1 {
  color: #302A43;
}
section.lead h2 {
  color: #302A43;
}
section.lead h3 {
  color: #302A43;
}
section.lead a {
  color: #302A43;
}
h1 {
  color: #302A43;
}
h1 strong{
  color: #302A43;
}
h2 {
  color: #F49326;
}
h2 strong{
  color: #F49326;
}
h3 {
  color: #94C74C;
}
h3 strong{
  color: #94C74C;
}
a {
  color: #302A43;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
table {
    width: 100%;
}
</style>

<!--
_class: lead
-->

# 2,5 ans de migrations en environnement "Cloud Native" quand on n'est ni une Startup, ni dans le CAC40

*Rémi Verchère @ Accenture*

<!--
---
## Au sommaire
- **REX** de ~ 2 ans de mission pour une ETI : *mon job ces derniers 24 mois*
- 3 domaines d'intervention pour aider les devs
  - 1 - Gestion de la **forge logicielle**
  - 2 - Déploiement des plateformes **kubernetes**
  - 3 - Mise à disposition d'**environnement de dev** & **CI/CD**
  - **Accompagnement** & **support** sur ces 3 domaines

## Disclaimer
- **Mon** expérience, **mon** avis (subjectif, mais forcément le bon)
- Quelques traits **forcés**
- Les devs on vous aime bien quand même ;)
-->

---
<!--
_transition: flip
-->

![bg fit right:35%](assets/photo-profil.png)
![bg fit right:35% 70%](assets/Acc_GT_Dimensional_RGB.svg)


# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

- Ops qui aide les Devs à mettre en prod

- 🫶 Open Source & CNCF landscape 

- Papa x 👦👦👧👧

- 🌐 *@rverchere*

- 📖 https://vrchr.fr/

---
<!--
_class: lead
-->

# Une ETI, mais laquelle ?

---
![bg fit 95%](assets/IRSN_logo_2020.svg.png)

---
# L'IRSN

**I**nstitut de **R**adioprotection & **S**ûreté **N**ucléaire

- Expert public en matière de recherche et d’expertise sur les **risques nucléaires** et **radiologiques**

- **Etablissement public** à caractère **industriel** et **commercial** (EPIC), placé sous la tutelle conjointe du ministre de la Transition écologique, du ministre des Armées, et des ministres chargés de la Transition énergétique, de la Recherche et de la Santé

- **1744 collaborateurs** à fin 2022 (ingénieurs, médecins, agronomes, vétérinaires, techniciens, experts et chercheurs)

---
# L'IRSN

## Missions

Missions d'expertise et de recherche

- **Surveillance radiologique** de l’environnement et intervention en situation d’urgence radiologique.
- **Radioprotection** de l’homme.
- **Prévention** des accidents majeurs dans les installations nucléaires.
- **Sûreté** des réacteurs.
- Sûreté des usines, des laboratoires, des transports et des déchets.
- **Expertise** nucléaire de défense.

---
# L'IRSN

## Système d'Information

- DSI: **35** personnes

- Application diverses & variées
  - Exercices de crise
  - Suivi des travailleurs exposés aux rayonnements ionisants
  - Données relatives au nucléaire & informations géographiques spatiales
  - Calculs scientifiques
  - Etc.

---
![bg fit 95%](assets/irsn-2.png)

---
![bg fit 95%](assets/irsn-4.png)

---
![bg fit 95%](assets/irsn-3.png)

---
<!--
_transition: flip
-->

![bg fit 95%](assets/irsn-1.png)

---
<!--
_class: lead
-->

# ETI & Migration "Cloud Native"

---
# Cloud Native
## Infrastructure & Application

![bg right:30% fit 90%](assets/cloud-native-infrastructure.jpg)

- Agilité
- Mise à l'échelle
- Elasticité
- Résilience
- DevOps & Automatisation
- Architecture Microservices 
- Services Clouds
- Conteneurs
- ➡️ [✅ La checklist ultime pour rendre vos applications cloud native !](https://www.youtube.com/watch?v=3s-gtziZ3UU)

<!-- Voir talk de Katia -->

---
# Migration "Cloud Native"

## Génèse à l'IRSN

- Fin de contrat d'infogérance "classique", coûteux
- Rappatriement On Prem ou direction le **Cloud** (FR) ?
- Applications déjà conteneurisées
  Choix Orchestration (Docker Swarm, **Kubernetes**) ?

![center width:600px](assets/meme-3.png)

---
# Migration "Cloud Native"

## Promesses attendues

- Maitrise des sources, Process de déploiement,
  Reproductibilité, Capacité de restauration
- Facilité & Gain de temps mises à jour,
  Autonomie des développeurs,
Travail en mode agile
- Homogénéisation des pratiques,
  Sécurité & Qualité
- Facturation à l’usage, Gain de coût
- 100 balles & un Mars©

![bg right:30% fill](assets/promesse.jpeg)

---
# Migration "Cloud Native"

## Stratégie
- **Eligibilité** : toute nouvelle version majeure ou application
- Conteneurs + "Macro" services + "12 factor"
- **TOUT** (ou presque) dans k8s (applis, BDD & plus si affinité)
- D'abord applis **non critiques**, jusqu'aux applis **HDS**

![center width:400px](assets/meme-container.png)

---
# Migration "Cloud Native"

## Organisation

![center height:480px width:900px](assets/acteurs-1.png)

---
# Migration "Cloud Native"

## Organisation

&nbsp;

![center](assets/acteurs-2.png)

![bg right:30% fit](assets/devops-sysadmin.jpg)


---
# Migration "Cloud Native"

## Domaines d'intervention

- Infrastructure : **Plateformes Applis**
  - Clusters Kubernetes
  - Forge Logicielle

- **Applications**
  - Accompagnement des devs
  - Support Infra & co

- 1,75 Ops pour gérer un peu tout ça 😅

![bg right:30% fill](assets/responsabilite.jpg)

---
<!--
_transition: flip
-->

# Migration "Cloud Native"

![center width:1200px](assets/planning.png)


---
<!--
_class: lead
-->

# Infrastructure

## Plateformes Applis,  Forge Logicielle

---
# Plateformes Applis

## Quelques chiffres

- 11 Projets Public Cloud @OVHcloud
- **21** clusters **M**anaged **K**ubernetes **S**ervice (non prod & prod)
- Entre 3 et 15 nodes par cluster
- Entre **100 et 500 pods** par cluster
- Autres composants Cloud :
  Buckets S3, Network, VMs

![bg right:42% 90%](assets/rancher-workloads.png)

---
# Plateformes Applis

## Infrastructure (très) simplifée

![center width:950px](assets/irsn-forgelogicielle.png)

---
# Plateformes Applis

![bg right:40% 90%](assets/k8s-app-meme.jpg)

- Ops s'occupent des applis "infra"
- Devs déploient "juste" l'appli
  *Charts Helm via CI/CD*

## Composants "Infra"

- Agent Rancher
- Ingress Controller
- Cert Manager
- Backup
- Monitoring & Logs <!-- On en parle après-->
- Policy Engine

---
# Plateformes Applis

##  Déploiement "Infra"

![bg right:45% 90%](assets/croissance-exponentielle.jpg)

- Phase de découverte :
Repasser sur les clusters
- Uniformité des versions
- Rythme de déploiement
- Pet vs Cattle
- ➡️ **Automatisation & Variabilisation**

---
# Plateformes Applis

## Comment on déploie ?

1. Depuis le manager OVHcloud, puis Helm pour les applis
2. Terraform pour **TOUT** ⚠️ Fausse bonne idée <!-- Gestion des modules, trop peu utilisé vs maintenance code, templating helm over tf -->
3. Terraform pour bootstrap, puis Helm pour les applis  ⬅️ *(90%)*
4. Terraform pour bootstrap, GitOps - POC sur les applis "infras" ⬅️  *(10%)* <!-- ArgoCD, Flux , maturité des équipes & réel gain VS plaisir techno -->

![width:500px](assets/gitops.drawio-1.png) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;![width:400px](assets/argocd.png)

---
# Plateformes Applis

## "Day2" -  Comment on met à jour ?
- Upgrade de version k8s & custom ressources <!-- `pluto`, ` kubent` -->
- Rythme d'upgrade des applications 'infra' ?
  - ➡️ A chaque upgrade k8s, sauf si faille sécurité
- Version supportée si besoin PRA ?

### Et les déploiements applicatifs ?

- Adaptation charts Helm applicatifs, qui gère ?
- ⚠️ Mise à jour des pipelines ! <!-- kubectl, helm -->

![bg right:27% fit](assets/update.gif)

---
# Plateformes Applis

## "Day2" - Comment on l'opère ?
- Accès au pods / nodes pour debug ?
- Accès aux BDD ? Et les dumps ?
- Copie de fichiers / données statiques ?
- Gestion des backups / restore ?
  - ➡️ Gestion de la partie "Stateful"
- Accès aux logs ?
- `FROM scratch` & `kubectl exec` ?

<!-- 💡 Mise en place de runbooks pour la MCO -->

![bg right:32% fit](assets/meme-2.jpg)

---
# Plateformes Applis

## Comment on sait que ça tourne ?

- Observabilité ! Métriques & Logs <!--  (*les traces on verra plus tard*) -->

![center width:590px](assets/observabilite-1.png)

<!-- - Prometheus, Loki, Grafana, Thanos ! -->

---
<!--
_transition: flip
-->

# Plateformes Applis

## On y voit plus clair !
- Erreurs classiques d'infra
- Suivi de la performance
- Suivi des sauvegardes 💡
- Suivi des déploiements 💡
- Suivi MKS & FinOps 💡💡<!-- Exporter OVH -->

![bg right:45% fit](assets/metrics-ovh.png)

---
# Forge Logicielle

- Un plateforme appli *particulière*, applications "tierces"

> *[...] permettre à plusieurs développeurs de participer ensemble au développement d'un ou plusieurs logiciels [...]*

![center width:750px](assets/irsn-forgelogicielle.png)

---
# Forge Logicielle

## Utilisation & Challenges
- ~650 utilisateurs, ~1K projets, ~250K pipelines
- Adoption de la plateforme : Disponibilité & Applications ➡️ **SPOF**
- Administration de la plateforme post-mission ?

### Une V1, puis une V2
- Composants obsolètes & stabilité
- Serveurs & Instances dédiés (RKE, Docker) ➡️ MKS (iso plateformes applis)
- Tout au même endroit ➡️ Split 3 clusters
- Utilisation de **Velero** (backup/restore) pour la migration 💡<!-- Cf talk Velero -->

---
<!--
_class: lead
-->

<style scoped>
section.lead {
  background-image: url("assets/fail-1.gif");
}
</style>

<!--
Déploiement:
- Maturité Providers Terraform
- Enroll Rancher pas idempotent

MaJ des clusters:
- MaJ clusters **AVANT** maj CRDs (ingress, )
- Support des versions Rancher vs cycle release MKS
- Majorité de charts Helm : cycle de release différent

En prod:
- Gestion des volumes & backups
- Scaling à 0 des nodes avec Kyverno

Forge Logicielle:
- Séparation des rôles applicatifs
- Puissance Worker Nodes vs Control Plane
- Driver CSI : restart des nodes, recréation des PVs
- **Harbor**, **Keycloak** & applis clientes sur le même cluster
  ***Bonus***: Kyverno
- Upgrade charts **Helm** qui se passent mal
-->

---
<!--
_class: lead
_transition: flip
-->

<style scoped>
section.lead {
  background-image: url("assets/success-1.gif");
}
</style>

<!--
- Homogénisation des composants, nomenclature
- Autonomie des devs sur les déploiements
- Ressources consommables facilement (CPU, RAM, Disque)
- Réplication Prod / Hors Prod à iso-périmètre

Forge Logicielle:
- Adoption de la plateforme, même pour les projets hors Kubernetes
- SSO: Gestion centralisée des comptes (délégation AD !)
-->

---
<!--
_class: lead
-->

# Applications &
# Accompagnement équipes dev

---
# Applications

## Organisation
- Développement d'applications via appel d'offre
  - Equipes différentes (build & run applicatif)
  - Technologies différentes (Java, PHP, JS)
  - Complexité & Temps de développement différents

![center width:500px](assets/acteurs-3.png)

---
# Accompagnement

## Mise à dispositon des environnements
- Accès à la **forge logicielle**
- **Boostrap** structure projet GitLab
- CI/CD: exemple de construction **conteneur & déploiement**
- CI/CD: **Templates** de jobs

![bg right:40% 90%](assets/gitlab-boostrap.png)

---
# Accompagnement

## Maturité des équipes dev
- Conteneurs & Kubernetes
- Intégration & Déploiement continu
- Développement != YAML <!-- Développeur != Expert CI/CD -->
- Débutants : formation, plusieurs fois
- Diffusion de "bonnes pratiques"

![bg right:42% fill](assets/docker-build.png)

---
# Accompagnement

## Communication
- **Multicanal**: tickets, MR, Chat, Mail
- Discussions: **directe**, décision Resp. de Projet
- Préconisation, pas d'obligation

## Mise en production
- Aide au déploiement -> **autonomie**
- Si on doit faire : on fait **avec** les équipes
- Analyse & Conseils, Support en production

![bg right:35% fill](assets/01189998819991197253-maurice-moss.gif)

---
<!--
_class: lead
-->

<style scoped>
section.lead {
  background-image: url("assets/fail-3.gif");
}
</style>

<!--
- Images Docker custom + obsoletes
- Logs sur volumes
- Oublie de données persistantes
- Perfs BDD à 2 semaines de la MEP, disque local
- Gestion BDD (flyway, etc)
- FTP qui n'a jamais marché
- HPA sur BDD
- Requests & Limits
- Variables en dur dans images Docker
-->

---
<!--
_transition: flip
_class: lead
-->

<style scoped>
section.lead {
  background-image: url("assets/success-3.gif");
}
</style>

<!--
- Equipes super autonomes, qui aident
- MR sur les templates de CI/CD
- Communication fluide / pas de pb d'ego, transparence
- Découverte d'un nouveau monde SIG: devs pas forcéments experts, mais excellent en ingénierie
- GPU & AI : challenge
-->

---
![bg height:720px width:1280px](assets/baby-police.gif)

---
<!--
_transition: flip
-->

# Sécurité !

## Comment on sécurise les accès & déploiements ?

- Restriction des droits, mais habitude des devs ? ➡️ **Communication**
- Règles sur la CI/CD, contournables ? ➡️ **Communication**
- Règles d'admission, mais applis déjà en prod ? <!-- kyverno, sympa l'upgrade --> ➡️ **Audit** 
- Gestion des Secrets ? ➡️ **Sécurité** vs **Utilisabilité**
- 💡 **Bug Bounty** : bon moyen pour mettre au propre

![center width:420px](assets/kyverno.png)

---
<!--
_class: lead
-->

# Et l'IA dans tout ça ?

---
# L'IA ?

## Données dosimétriques 'sensibles'

- Impossible d'utiliser OpenAI 🤷‍♀️

- Equipes Data : Utilisation instances avec GPUs sur clusters applicatifs

- Problèmes de dispo & partages de ressources...

## Manque de temps pour aujourd'hui, une autre fois ?

➡️ Talk de Stéphane Phillipart après la pause !

---
<!--
_class: lead
-->

# Bilan & La suite

---
![bg right:40%](assets/yes-baby-meme-6.jpg)

# Promesses tenues ?

- Maitrise des sources 🟢
  Process de déploiement 🟢
  Reproductibilité 🟢
  Capacité de restauration 🟢
- Facilité & Gain de temps maj 🟢
  Autonomie des développeurs 🟢
  Travail en mode agile 🟢
- Homogénéisation des pratiques 🟡
  Sécurité & Qualité 🟡
- Facturation à l’usage 🟠
  Gain de coût 🟠

---
# Bilan 

## Obstacles rencontrés
- Montée en compétences des équipes 
- Compétences des devs sur l'environnement cible
- Hype technologique vs besoin client
- Intervention parfois tard dans le process de déploiement
- Limite de responsabilité : métier vs infra

![bg right:40% fit](assets/meme-5.png)

---
# Bilan

## Si c'était à refaire
- **OUI**, avec plus de compagnonnage & tutos pour les équipes métiers.
- Mieux travailler sur le partage des responsabilité entre Dev et Ops
- Sécurité & bonnes pratiques au plus tôt des projets

![bg right:40% fit](assets/meme-4.png)

---
<!--
_class: lead
-->

# Merci !

### On s'éclate bien sur des migrations "Cloud Native"
### même si on n'est ni une Startup, ni dans le CAC40 !

*Rémi Verchère @ Accenture*
