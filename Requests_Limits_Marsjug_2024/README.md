---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<!--
Lorsqu'on doit déployer une application sur un cluster Kubernetes, une bonne pratique est de définir des ressources "requests" et "limits" pour garantir le bon fonctionnement de celle-ci tout en garantissant la bonne santé du cluster qui l'accueille.

OK, mais quelles valeurs de "requests" et "limits" doit-on spécifier ? Pas assez de RAM et l'application sera "OOMKilled" ? Trop de CPU mais cela bloquera les autres déploiements ?

Après un tour d'horizon des différentes options pour paramétrer ces ressources, je vous présenterai un outil simple mais efficace pour vous aider à définir des valeurs pragmatiques : Goldilocks et l'utilisation automatique des Vertical Pod Autoscaler.
-->

<style>
section {
    background-color: #fff;
    color: #000; 
    font-size: 28px;
}
section.lead {
    background-color: #333;
    background-image: "";
    color: #fff; 
}
section.lead h1 {
  color: #fff;
}
section.lead h2 {
  color: #fff;
}
section.lead a {
  color: #ccc;
}
h1 {
  color: #381b45;
}
h2 {
  color: #55c4f0;
}
h3 {
  color: #e72d4a;
}
a {
  color: #381b45;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
table {
    width: 100%;
}
</style>

<!--
_class: lead
-->

![bg 90% left:25%](assets/marsjug-logo.png)

# Dimensionnez correctement vos déploiements Kubernetes

*Rémi Verchère @ Accenture*

<!--
_footer: https://presentations.verchere.fr/Requests_Limits_Marsjug_2024/
-->

---
# Répondre à la question ⤵️

> **Quelles Requests & Limits définir sur vos applications conteneurisées ?**

# Agenda

- (R)appel **Requests & Limits** dans **Kubernetes**

- Présentation de **2 outils (+ démo)** pour vous aider à les définir

- **Stratégies** de mise à l'échelle : focus sur **Vertical Pod Autoscaler**

- Q/A si on a du temps

<!--
_footer: Crédits : beaucoup d’images de chez Sysdig, Datadog & Robusta qui expliquent bien les choses.
-->

---
<!--
_transition: flip
-->

![bg fit right:35%](assets/photo-profil.png)
![bg fit right:35% 70%](assets/Acc_GT_Dimensional_RGB.svg)

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud Native Infrastructure Consultant

- Ops qui aide les Devs à mettre en prod

🫶 Open Source & CNCF landscape

- Papa x 👦👦👧👧

🌐 *@rverchere*

---
<!--
_class: lead
-->

# (R)appel Requests & Limits

---
# Requests & Limits

## Requests
Ressources nécessaires pour placer un conteneur sur un noeud.

## Limits
Ressources maximum qu'un conteneur peut utiliser sur un noeud.

## Sur **Memory** & **CPU** (📺)

![bg right:45% fit](assets/Kubernetes-Limits-and-Request-05-1170x828.png)

<!--
The Miserable Life of a CPU Instruction - Samuel Ortiz
https://www.youtube.com/watch?v=j6-yGGBTXGU
-->

<!--
_footer: https://sysdig.com/blog/kubernetes-limits-requests/
-->

---
# Requests & Limits

## Qualité de Service

- Ni Requests, ni Limits : **Best Effort**
- Requests < Limits : **Burstable**
- Requests = Limits : **Guaranteed**

## PDB, Evictions, Priority Classes

- Ce n'est pas le sujet aujourd'hui...

![bg right:36% fit](assets/14fig04_alt.jpg)

<!--
_footer: https://livebook.manning.com/concept/kubernetes/qos-class
-->

---
![bg fit](assets/kubernetes-resources-cheatsheet-1170x585.png)

---
# Requests & Limits

Quel sizing alors ? Trop ? Trop peu ?

![width:600px](assets/Millions-wasted-image2-1170x829.png)

![bg right:40% fit](assets/Millions-wasted-image1-768x805.png)

<!--
_footer: https://sysdig.com/blog/millions-wasted-kubernetes
-->

---
# Requests & Limits

![center width:950px](assets/2023-container-report-graphics-fact-5.png)

<!--
_footer: https://www.datadoghq.com/container-report/#5
-->

---
# Requests & Limits

![center width:950px](assets/2023-container-report-graphics-fact-5a.png)

<!--
_footer: https://www.datadoghq.com/container-report/#5
-->

---
# Requests & Limits

![center width:950px](assets/fairwinds_report.png)


<!--
_footer: https://www.fairwinds.com/news/2024-kubernetes-benchmark-report
-->

---
<!--
_transition: flip
-->
# Requests & Limits

## Ça n'arrive pas qu'aux autres...

![center width:950px](assets/mail-requests.png)

---
<!--
_class: lead
-->

# Aide au dimensionnement

---
# Aide au dimensionnement

## Plusieurs méthodes

1) Au doigt mouillé itératif ☝️
2) Spécial devs Java ⤴️ 🧌
3) Grâce aux métriques 🔍 !

<!--
1. Standards : Prometheus, VictoriaMetrics, Datadog, etc.
2. Plus "avancées" : VPA
-->

---
<!--
_transition: flip
-->

# Aide au dimensionnement

## Plusieurs méthodes

1. Au doigt mouillé itératif ☝️
2. Spécial devs Java ⤴️ 🧌
3. Grâce aux métriques 🔍 !

<!--
1. Standards : Prometheus, VictoriaMetrics, Datadog, etc.
2. Plus "avancées" : VPA
-->

## Outils pour vous faciliter la vie
1. Krr
2. Goldilocks

---
# Krr

- From Robusta : https://github.com/robusta-dev/krr
- "Prometheus-based Kubernetes Resource Recommendations"
- CLI, sans agent, basé sur métriques Prometheus & co, extensible
  *Possibilité run in-cluster périodique (notifs Slack)*
- Calcul : 👀 https://github.com/robusta-dev/krr#algorithm

![center width:1200px](assets/krr_example.png)

---
<!--
_transition: flip
-->
# Krr

## Démo !

![center width:780px](assets/demo.gif)

---
<!--
_transition: flip
-->

# Goldilocks

- From Fairwinds : https://goldilocks.docs.fairwinds.com/ 
- "Get your resource requests 'Just Right'"
- Automatise les "**V**ertical **P**od **A**utoscalers" en mode recommendation (⁉️),
- Interface Web

![center width:550px](assets/goldilocks.png)

---
<!--
_class: lead
-->

# VPA - Vertical Pod Autoscaler

---
# VPA

- Adaptation automatique des Requests & Limits du pod

![center width:1100px](assets/vpa-diagram.png)

<!--
_footer: https://www.kubecost.com/kubernetes-autoscaling/kubernetes-vpa/
-->

---
# VPA

- 3 composants
  - **Recommender**
  - Updater
  - Admission Controller
- Plusieurs modes
  - Recreate
  - Initial
  - **Off**

![bg right:60% fit](assets/vpa-allocate-resources.png)

<!--
_footer: https://www.kubecost.com/kubernetes-autoscaling/kubernetes-vpa/
-->

---
# VPA

## Remarques

- Pas par défaut, à **installer**
- Besoin de créer une **ressource** de type “VerticalPodAutoscaler”
- ⛔ A ne pas utiliser avec HPA
- **Restart** du conteneur ★
- Fournit des **métriques** : via le '*status*' de la resource VPA
- **Calcul** : 👀 [vertical-pod-autoscaler.md#recommendation-model](https://github.com/kubernetes/design-proposals-archive/blob/main/autoscaling/vertical-pod-autoscaler.md#recommendation-model) 🤯
   - Requests : CPU 90% Percentile par défaut, RAM OOMKilled
   - Limits : Ratio Requests Initiaux

---
# VPA

## Exemple

```yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
metadata:
  name: my-app
spec:
  targetRef:
    apiVersion: apps/v1
    kind: Deployment
    name: my-app
  updatePolicy:
    updateMode: "Off"
status:
  recommendation:
    containerRecommendations:
    - [...]
```

---
# Autres types d'Autoscalers

## 1) **H**orizontal **P**od **A**ustocaler

![center width:400px](assets/2023-container-report-graphics-fact-4.png)

## 2) **C**luster **A**utoscaler

## 3) **Mu**lti-dimensional **P**od **A**utoscaling
- AEP-5342 (Autoscaler Enhancement Proposal): Multi-dimensional Pod Autoscaler

---
<!--
_transition: flip
-->

# Autres types d'Autoscalers

## Un mot sur les HPA

- De **plus en plus** utilisés

- Déclenchement:
  - Par défaut sur CPU/RAM
    (metrics-server)
  - Autres métriques via Prometheus
    (net latency)

- Temps de réaction ? Serverless / Event Driven
   - Voir “Keda”, “Knative”, …

- 🤔, mais sur quoi se déclenchent les HPAs ?

![bg right:38% fit](assets/hpa-overview.png)


---
# Goldilocks

## Démo !

![center width:780px](assets/demo.gif)

---
# Goldilocks

## C'est moche hein ?

![center width:780px](assets/goldilocks.png)

---
# Goldilocks

## On peut faire "moins moche..."

- Parlons Kube-State-Metrics, mieux : CustomResourceStateMetrics !!

```yaml
# kubectl get vpa goldilocks-example -o yaml
apiVersion: autoscaling.k8s.io/v1
kind: VerticalPodAutoscaler
[...]
status:
  recommendation:
    containerRecommendations:
    - target:
        cpu: 15m
        memory: "1238659775"
```

- Pour en savoir plus, venez jeudi au meetup "DevOps Aix-Marseille" !

<!--
_footer: https://www.vrchr.fr/posts/2024/2024-01-09-kube-state-metrics-vpa/
-->

---
<!--
_transition: flip
-->

# Goldilocks + VPA + Grafana = ❤️

![center width:1130px](assets/grafana_vpa.png)

---
<!--
_transition: flip
-->

# Mes stratégies

## Mes Outils
- Goldilocks + VPA
- Prometheus + Grafana + CustomResourceStateMetrics

##  Requests, Limits ?
- CPU Requests, No Limits
- RAM Requests = Limits
- ➡️ Ce dont l'application à **besoin** ★

<!--
- Requests trop basses & HPA
-->

![bg right:40% fit](assets/requests-limits.png)

<!--
_footer: https://home.robusta.dev/blog/stop-using-cpu-limits
-->

---
# Ressources

## Blog posts

- [Kubernetes CPU Limits (cgroup quotas)](https://sysdig.com/blog/kubernetes-cpu-requests-limits-autoscaling/#kubernetes-cpu-limits)

- [Stop setting CPU & Memory Requests](https://thenewstack.io/stop-setting-cpu-and-memory-requests-in-kubernetes/)

- [Stop Using CPU limits](https://home.robusta.dev/blog/stop-using-cpu-limits)

- [The Case for Kubernetes Resource Limits: Predictability vs. Efficiency](https://kubernetes.io/blog/2023/11/16/the-case-for-kubernetes-resource-limits/)

- [Why does my 2vCPU application run faster in a VM than in a container?](https://hwchiu.medium.com/why-does-my-2vcpu-application-run-faster-in-a-vm-than-in-a-container-6438ffaba245)
---
<!--
_transition: flip
-->

# Nouveautés depuis K8S 1.27

- **In-place Resource Resize for Kubernetes Pods (alpha)**
  "*In Kubernetes v1.27, we have added a new alpha feature that allows users to resize CPU/memory resources allocated to pods without restarting the containers*"

- **HorizontalPodAutoscaler ContainerResource type metric moves to beta**
  "*Kubernetes 1.20 introduced the ContainerResource type metric in HorizontalPodAutoscaler (HPA). In Kubernetes 1.27, this feature moves to beta and the corresponding feature gate (HPAContainerMetrics) gets enabled by default.*"

- **Quality-of-Service for Memory Resources (alpha)**
  <!-- "*Support for Memory QoS was initially added in Kubernetes v1.22, and later some limitations around the formula for calculating memory.high were identified. These limitations are addressed in Kubernetes v1.27*" -->

---
<!--
_class: lead
-->

![bg left:25% fit](assets/marsjug-logo.png)

# Merci !

*Rémi Verchère @ Accenture*

<!--
_footer: https://presentations.verchere.fr/Requests_Limits_Marsjug_2024/
-->

---
<!--
_class: lead
-->

# Kubetrain

---
# Kubetrain

KubeTrain.io vise à permettre aux participants de la prochaine Cloud Native Computing Foundation (CNCF) #kubecon EU 2024 à paris de partager le voyage en train au départ de différentes villes d'Europe.

Plus d'infos sur le site [kubetrain.io](kubetrain.io)

![bg right:40% 90%](assets/kubetrain-01.png)

---
# Kubetrain - Objectif

L'objectif ? pouvoir réserver un wagon qui permettra à tous les participants venant d'Aix-en-Provence ou de Marseille de faire le déplacement à Paris ensembles.

Cela nous permet de :
- réduire notre impact sur l'environnement
- obtenir des tarifs préférentiels
- échanger pendant le trajet (si vous en avez envie)
- joindre l'utile à l'agréable

Les billets sont disponibles sur [https://kutt.it/kubetrain](https://kutt.it/kubetrain)
Page kubetrain : [https://kubetrain.io/departures/aix-marseille/](https://kubetrain.io/departures/aix-marseille/)
Référent local : Frédéric Léger / webofmars

![bg right:20% fit](assets/kubetrain-02.png)

---
# Kubetrain - Sponsoring 1/2

Le projet est en recherche de sponsoring afin de réduire les coûts de transport et pouvoir organiser un événement sur place (mais chut ! 🤐)

## Quels avantages à être sponsor ?

- 2 tickets de train à utiliser comme vous voulez (tech ou marketing)
- Visibilité pour votre marque
- Reconnaissance de toute la communauté tech Aix-Marseille
- Probablement une tonne de points de karma 😀

![bg right:20% 90%](assets/kubetrain-03.png)

---
# Kubetrain - Sponsoring 2/2

## Mais aussi

- Faire le voyage avec des développeurs, des ops, des devops, des tech de la région
- Le nom et logo de votre société sur le coupon d'échange
- Diffuser les messages de votre choix durant le trajet
- Distribution de vos goodies si vous le souhaitez
- Votre logo sur différentes pages du site [kubetrain.io](https://kubetrain.io)

![bg right:20% 90%](assets/kubetrain-03.png)

---
<!--
_class: lead
-->

![bg left:25% fit](assets/marsjug-logo.png)

# Encore merci !

*Rémi Verchère @ Accenture*
