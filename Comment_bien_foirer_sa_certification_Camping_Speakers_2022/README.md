---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<style>
section {
    background-color: #fff;
    background-image: none;
    color: #000; 
    font-family: "Graphik Black","Arial",Sans-Serif;
    font-size: 30px;
}
h1 {
  color: #a100ff;
}
h2 {
  color: #7500c0;
}
h3 {
  color: #460073;
}
a {
  color: #96968c;
}
img[alt~="center"] {
  display: block;
  margin: 0 auto;
}
footer {
  font-size: 14px;
}
section.lead {
  background-color: #000;
  background-image: none;
  color: #fff; 
}
section.lead h1 {
  color: #a100ff;
}
section.lead h2 {
  color: #96968c;
}
section::after {
  font-size: 14px;
}
</style>

<!--
class: lead
-->
![bg fit left:30% 90%](assets/logo_camping_speakers.png)

# Comment bien foirer
# vos certifications
# Kubernetes CK{A,S}

<!--- Et plus si affinités --->

---
<!--
footer: Camping des Speakers - 10/06/2022
-->
<style scoped>
section {
  background-color: #fff;
}
</style>

# Avant de commencer...

---

# La Cuisine m'a sauvé du Burn-out
## Yannick Guern @ Clever Cloud

---
# Le pire talk de ma vie
## Jordane Grenat @ Comet Meetings

---
# Les 5 choses que j'aurais aimé savoir plus tôt dans ma carrière
## Yohan Lasorsa @ Microsoft

---
# Trucs et astuces pour réussir
# son burn-out
## Cynthia Staebler & Julia Lehoux @ Zenika

---
# Encore une présentation d'échec !
## ❤️‍🔥 Bienvenue au Camping des Dépressifs ❤️‍🔥
### En toute bienveillance biensûr ;)

---
<!--
class:
-->

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud & DevOps Consultant

- Ops qui aime bien les Devs

- Papa x 👦👦👧👧

- Certifié x ☸️☸️☸️☸️☸️☸️

🌐 *@rverchere*

![bg fit right 80%](assets/photo-profil.png)
![bg fit right 66%](assets/Acc_GT_Dimensional_RGB.svg)

---
![bg 50%](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_01.png)

<!--
En début d’année 2021, j’ai tenté la Certification Kubernetes Administrator (CKA), et j’ai échoué au premier essai.

Cette année (2022), j’ai également tenté la Certification Kubernetes Security Specialist (CKS), et j’ai à nouveau échoué au premier essai !

Avec ces expériences acquises sur le “fail” de certifications, je vous propose quelques astuces pour mettre toutes les chances de votre côté et échouer ! Lisez alors avec attention ce qui suit, à vos risques et périls !
-->

---
<!--
class: lead
-->

# Avant / Préparation de l’examen

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #1 : Trop de confiance

<!--
Nous étions quelques-uns à se motiver pour passer la certification. Quelques amis et collègues ont réussi l’examen haut la main, même certains dont je ne savais même pas que le sujet les intéressait. Si eux ont réussi, pourquoi pas moi ? En plus, ils n’ont jamais utilisé Kubernetes en production !

Premier constat : OK, ce ne sont peut être pas des experts, mais ils ont travaillé dur, bossé consciencieusement, et sont peut-être aussi habitués à passer ce genre de certifcations, pas moi ! C’est un bon point de départ si vous vous considérer suffisamment bon pour ne pas réviser et être studieux, c’est un bon début pour prévoir l’échec !
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #2 : Ne pratiquez pas

<!--
La CKA et CKS sont des examens de mise en situation avec de la pratique. Entre 15 et 20 questions de type lab, pas de QCM ici. Vous devez alors être à l’aise avec la CLI et les commandes kubectl, comme par exemple:

* créer un pod
* mettre à l’échelle un deployment
* configurer l’api-server
* créer des network-policies
* éditer un paquet de fichiers yaml
* etc.

Si vous ne savez pas le faire instinctivement, tant mieux, quelques recherches sur Internet pour savoir de quoi on parle devrait suffire
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #3 : N’apprenez ni `vim` ni `kubectl`

<!--
Pour être plus efficace, il existe des raccourcis sous vim et kubectl, qui vous font gagner un temps précieux (indentation yaml, forcer l’arrêt des conteneurs, etc).

Egalement, vous pouvez générer des manifests yaml avec l’option --dry-run pour avoir déjà une base pré-remplie.

Vous ne devez pas les utiliser, il ne faudrait pas que vous gagniez du temps sur la suppression de ressources, ou l’édition de fichiers.
-->

---
<!--
_footer: ""
-->
![bg fit 80%](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_06.png)


---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #4 : Ne vous entrainez pas 
# sur `killer.sh`

<!--
Depuis l’été 2021, la Linux Foundation offre 2 examens blancs gratuits sur la plateforme killer.sh, qui sont plus difficiles que l’examen réel.

Ces 2 sessions sont les mêmes, et vous avez 36 heures pour chacune d’entre elles pour profiter d’aller chercher un maximum d’informations et détails sur les questions posées, et vérifier que tout fonctionne comme vous l’avez compris. Les réponses sont explicitées si vous n’y arrivez pas.

Ne l’utilisez surtout pas alors, vous pourriez apprendre des choses intéressantes !
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #5 : Ne faites pas de bookmarks sur kubernetes.io

<!--
Dans les directives de l’examen, il est possible d’avoir un onglet supplémentaire de votre navigateur ouvert pour rechercher de la documentation et exemples sur le site officiel kubernetes.io. Ne l’utilisez simplement pas.

Bon OK, ça vous démange, vous êtes quand même curieux, vous voulez y jeter un oeil. Ça me va, mais assurez-vous de ne pas avoir parcouru le site ni prévu de bookmarks sur les éléments clés de la certification (vous trouverez quelques bookmarks déjà tout prêts sur Internet, mais ne les utilisez pas vous risqueriez de réussir).
-->


---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #6 : Utilisez votre 1er essai
# le plus tard possible

<!--
Une fois la certification achetée, vous avez 1 an pour la passer, et 2 essais. Le 2ème essai doit être passé dans l’année. Vous pouvez alors tenter une première fois la certif, échouer, la retenter une deuxième fois, et échouer à nouveau, cela fait un beau combo !

Pourquoi se faire autant de mal, et simplement échouer définitivement lors du premier essai, en passant la certification le plus tard possible ? Mon conseil : planifier la date du premier essai le plus tard possible, ainsi il vous sera impossible d’utiliser votre “free retake”. Quoi de plus facile ?
-->

---
<!--
class:
footer: Camping des Speakers - 10/06/2022
-->
<style scoped>
section {
  background-color: #fff;
}
</style>

# #7 : Oubliez que vous avez souscrit à l’examen

![bg fit 80%](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_04.png)

<!--
Celle-ci est facile: achetez un bon pour passer la CKS, travaillez sur pleins d’autres sujets qui n’ont rien à voir pendant l’année, et d’un coup recevez un mail qui vous informe qu’il ne vous reste que quelques jours pour passer la certif !

C’est ce qui m’est arrivé… Seulement 1 mois pour préparer l’examen, en période de fêtes de fin d’année, avec le travail quotidien, la gestion familiale, et le 2ème essai inclus !
-->

---
<!--
class: lead
-->
<style scoped>
section {
  background-color: #fff;
}
</style>

# #8 : Ne vous mettez pas dans des conditions d’examen

<!--
La CKA a été mon premier examen de la vie professionnelle après mes diplômes universitaires, “quelques” années plus tôt. J’ai oublié comment cela pouvait être stressant. Quelques éléments ci-après pour ne pas vous mettre en conditions de passsage d’examen :

* Ne dormez pas, ou peu, et faites tout ce que vous pouvez pour être le plus nerveux possible.
* Ne nettoyez pas votre bureau / pièce, étant donné qu’on vous le demandera le jour J
* Prévoyez une journée de travail arrassante la veille (une bonne mise en prod bien pourrie) afin d’être le plus fatigué possible (essayez aussi de planifier l’exam tard le soir, quite à être fatigué…)
* Si vous avez des enfants, en période COVID faites en sorte qu’ils soient malades pendant vos révisions et examen (ce n’est pas une blague, ça m’est arrivé !)
-->

---
<!--
footer: ""
-->

![bg 75%](assets/IMG_20200229_164345.jpg)

---
![bg fit](assets/IMG_20200512_090012.jpg)

---
![bg 80%](assets/IMG_20220330_181200.jpg)

---
<!--
footer: Camping des Speakers - 10/06/2022
-->
# Pendant l’examen

<!--
Ca y est, on y est, c’est LE jour J, plus que quelques instants et vous allez le rater cet examen ! Dans le cas où vous n’êtes pas 100% sûr de vous, quelques conseils supplémentaires ci-après.
-->

---
<!--
_footer: ""
-->

![bg fit 80%](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_02.png)

---
<!--
class: lead
-->
<style scoped>
section {
  background-color: #fff;
}
</style>

# #8 (bis) : Ne vous mettez pas dans des conditions d’examen

<!--
* Ne nettoyez pas votre bureau / pièce, étant donné qu’on vous le demandera le jour J
* Utilisez une webcam la plus bas de gamme possible, ainsi l’examinateur ne pourra pas vérifier votre identité
* Ayez une connexion Internet instable (tel 4g / appel), double cam
* Utilisez un navigateur exotique (enrollment VMware)
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #9 : Ne lisez pas les questions en entier

<!--
Parfois, des indications vous facilitant la vie peuvent être écrites à la fin des questions. Le sachant, surtout évitez de les lire, faite ce qu’on vous demande ligne après ligne.
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #10 : N’utilisez pas les contextes k8s

<!--
Pour chaque question, on vous indique sur quel cluster il faudra intervenir, avec des contextes différents. Par exemple, on vous demande de corriger un déploiement sur le cluster B, mais vous étiez sur le cluster A lors de la question précédente.

Si vous ne changez pas de contexte, vous aurez 100% de chance de ne pas trouver le déploiement qui pose problème !

Cela parait trivial, mais j’ai perdu beaucoup de temps à cause du changement de contexte lors de ma première tentative CKA (oui, j’étais particulièrement stressé).
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #11 : Ne sauvegardez pas votre travail

<!--
Lorsqu’on vous demande de créer des déploiements ou tout autre ressource kubernetes, vous pouvez générer le manifest yaml pour l’adapter, le rejouer plus tard, si jamais vous vous êtes trompé et que vous souhaitiez corriger le tir.

Inutile de garder ces fichiers dans un coin, pourquoi revenir sur quelque chose qu’on pense faux, ce sera ça en plus pour louper la certif.
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #12 : Prenez votre temps

<!--
L’examen dure deux heures. Ce n’est pas la course, prenez votre temps. En étant optimiste, l’examen comporte 15 questions, et vous devez avoir un score d’au moins 68%… Soit au minimum 11 questions justes. Sur 2 heures, cela revient à 11 minutes par question, vous êtes larges !
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #13 : Lorsque c’est terminé,
# arrêtez tout, tout de suite

<!--
Dans le cas ou vous n’auriez pas pris en compte le conseil précédent, que vous avez répondu à toutes les questions et donc il vous reste du temps, n’en perdez pas d’avantage ! Ne vous relisez pas, ne retestez pas vos manifests, c’est inutile. Demandez simplement à l’examinateur de clore la session le plus vite possible, et fuyez !
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>

# #14 : C'est tout !
## Et oui, c'est tout ;)

---
# Fin de l’histoire

<!--
Avec tous ces tips, j’espère que vous échouerez aussi bien que moi. Cela dit, si vous ne voulez pas, il suffit simplement d’appliquer l’inverse de ce que j’ai écrit précédemment ;)

Heureusement, après toutes mes erreurs et échecs, j’ai été plutôt contrarié et frustré. J’ai alors décidé d’utiliser mon deuxième essai la semaine suivant ma première certification : j’étais déjà dans des conditions d’examens, je sortai de plusieurs jours d’apprentissage et révisions intensifs, et je savais à peu près où j’avais échoué et j’ai pu travailler sur les sujets que je ne maitrisais pas assez ou n’étais pas assez clairs

Je peux alors résumer mes échecs ainsi pour chaque certification :

* CKA: Pas assez de préparation et entrainements en CLI. Aussi une surdose de confiance en moi pour la CKA, celle-ci étant ma première certif depuis l’université

* CKS: Pas assez de préparation (j’insiste) et de temps, la période pour passer l’examen arrivait à expiration assez vite, et je savais pertinament que j’allais la rater. Mais j’ai pris ce premier essai comme une opportunité, pour me préparer le mieux que possible, pour ensuite réussir au deuxième essai.

Ainsi, je peux le dire maintenant, je suis certificé CKA et CKS, et tout cette préparation en vallait le coup !
-->
---
<style scoped>
section {
  background-color: #fff;
}
</style>

# Ce que j'ai appris
![center](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_03.png)

<!--
Evidemment, j’espère que vous ne prendrez pas mes conseils au pied de la lettre, et ferez l’inverse de ce que j’ai écrit si vous souhaitez réellement réussir vos examens CK{A,S}.

Ces échecs sont parfois nécessaires, nous rappelant qu’on peut se tromper. Mais apprendre de ces échecs, cela fait partie de la vie ! Ne vous sentez pas inférieurs lorsque vous lisez toutes ces success-stories sur les réseaux sociaux, tout le monde échoue un jour où l’autre !

Mes derniers mots seront de vous souhaiter bonne chance pour vos certifications. J’espère que vous n’échouerez pas, enfin, pas trop ;)
-->

---
<style scoped>
section {
  background-color: #fff;
}
</style>
# Post Scriptum

![center](assets/2022-01-24-comment-bien-foirer-vos-certifications-kubernetes_05.png)

<!--
Depuis l’écriture de cet article, et le passage réussi de la CKS, j’ai profité d’un bureau bien rangé, d’une mise en condition d’examen et d’un week-end tranquille sans les enfants pour tenter la CKAD : cette fois-ci je l’ai eu du premier coup, avec un score honorable de 98% ;)
-->

---
# Articles

- Anglais : https://medium.com/linkbynet/how-to-fail-your-ck-a-s-kubernetes-certifications-6151317bece6
- Français : https://presentations.verchere.fr/Comment_bien_foirer_sa_certification

---
# Merci !
## https://2022.devops-dday.com

