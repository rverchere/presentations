---
marp: true
theme: gaia
markdown.marp.enableHtml: true

---
<!--
Vous souhaitez contribuer à l’open source mais vous n’osez pas franchir le pas ? Votre boite n’a rien d’un éditeur de solutions open source ? Et alors ?! Nous verrons, par un retour d’expérience au sein de diverses sociétés, comment contribuer, chacun à son niveau, à l’open source et aux logiciels libres. Divers aspects seront abordés : le contexte d’entreprise, le fait de pouvoir contribuer ou pas, et dans quelles mesures, les limites pro/perso. Enfin, nous parlerons du côté business, comment en tirer partie, car nous savons tous que libre ne veut pas dire gratuit !
-->

<style>
section {
    background-color: #fff;
    color: #000; 
    font-family: 'Montserrat', 'Roboto', 'Open Sans';
    font-size: 30px;
}
h2 {
  color: #4059ff;
}
h3 {
  color: #579eff;
}
a {
  color: #401cff;
}
footer {
  font-size: 14px;
}
section.lead {
    background-color: #000;
    background-image: "";
    color: #fff; 
}
section.lead h1 {
  color: #fff;
}
section::after {
  font-size: 14px;
}
</style>

<!-- _class: lead -->
![bg fit left:25%](assets/logo_osxp_aplat-261x250.png)

# Comment contribuer à l’Open Source (*ou pas*) sans être éditeur de solutions Open Source

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Qui suis-je ?

👋 **Rémi Verchère**

💻 Cloud & DevOps Consultant

- Libriste *pragmatique*

- Ops qui aime bien les Devs

- Contributeur, quand je peux

- Papa x 👦👦👧👧

🌐 *@rverchere*

![bg fit right:45%](assets/photo-profil.png)
![bg fit](assets/LinkBynet-Part-of-Accenture.jpg)

---
# Pourquoi cette présentation ?

*Comment contribuer à l’Open Source (*ou pas*) sans être éditeur de solutions Open Source.*

- Proposition de mes collègues de donner un **Retour d'Expérience**

- Avec plus de **20 ans** à naviguer autour du Logiciel Libre,<br/> il y a surement des choses à dire !

- Bon moyen pour **contribuer**

- ***Challenge personnel***, et l'occasion de **vous** remercier

- Légitimé ? Sûrement...

---
<!--
_class: lead
paginate: false
footer: ''
-->

![bg fit left:25%](assets/logo_osxp_aplat-261x250.png)

# 4 entreprises,<br />4+ postes,<br />4++ façons de contribuer

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Contexte professionnel

- 4 exemples d'**entreprises**, divers postes

- **Maturité** des entreprises & lien avec l'OpenSource

- **Limites** à la contribution

- Quels **moyens de contribution** plus ou moins techniques

- **Bénéfices** apportés

- Interprétation **personnelle**, temporalité

![bg fit right:30%](assets/industry.gif)

---
# Entreprise n°1 ~ *2006-2009*
![bg fit right:25%](assets/sagemcom.png)
<!--  09 novembre 2021
16:00 - 16:45 : Comment de grands groupes collaborent autour de l’Open Source -->
- **Contexte** : Boite High-Tech, Triple Play GW 🌐

- **Env. Tech** : Logiciel Embarqué, 💾 Intégration

- **Maturité** : Forte connaissance du sujet <!-- (équipe dédiée) -->

- **Limites de contribution** :
  - Divulgation de projets R&D ?
  ![width:250px](assets/bbox2.png)

---
# Entreprise n°1 ~ *2006-2009*
![bg fit right:25%](assets/sagemcom.png)

- **Type de contributions tech** :
  - Patchs Kernel 🐧
  - Stack VoIP 🎤

* **Faits marquants** :
  - Obligation de mise à dispo du code

* **Bénéfices** :
  - Pour l'employeur : Mise en conformité, Visibilité
  - Pour l'employé : Enrichissement <!--- trop jeune pour m'en rendre compte -->

---
# Entreprise n°2 ~ *2009-2017*
![bg fit right:25%](assets/neotion.jpg)

- **Contexte** : Boite High-Tech, R&D TV Num 📺 <!-- Voir SIDO mon ancien chef -->

- **Env. Tech** : Logiciel Embarqué, Intégration, Sysadmin Debian & Management

- **Maturité** : *Pas contre*, ↗️ prise de conscience

- **Limites de contribution** :
  - Sur temps "libre" ⚠️, tant que ça ne gêne pas le business
  - Sur produits annexes (contamination)

---
# Entreprise n°2 ~ *2009-2017*
![bg fit right:25%](assets/neotion.jpg)

- **Type de contributions tech** :
  - Code Contribution : Kernel Linux 🐧
  - Code & Project Maintainer : Trac, Pyftdi <!-- forge logicielle -->
  - Code Maintainer : Exporter Prometheus <!-- bel exemple -->
  - Maintainer : 📦 Packaging Debian
  ![](assets/packaging-debian.png)<!-- qui a inventé ça ?! -->

---
# Entreprise n°2 ~ *2009-2017*
![bg fit right:25%](assets/neotion.jpg)

- **Type de contributions moins tech** :
  - Support financier : ❤️ Rudder
  - Sensibilisation juridique aux LL ⚖️
  avec Alterway
  <!--  10 novembre 2021 10:30 - 10:50 : Pour une approche ouverte, mutualisée et outillée de la conformité Open Source – projet Hermine / Camille Moulin -->

* **Faits marquants** :
  - Arrivée de Github 🚀

* **Bénéfices** :
  - Pour l'employeur : N/A
  - Pour l'employé : Enrichissement, Employabilité

---
# Entreprise n°3 ~ *2018-2021*
![bg fit right:25%](assets/axians.jpg)

- **Contexte** : ESN, Infrastructure IT ☁️

- **Env. Tech** : Consulting Open Source & Monitoring

- **Maturité** : Open Source as Business 🙃

- **Limites de contribution** :
  - Peu de limites si temps raisonnable
  - Hors production (*inter-contrat*)
  - Entendu avec le client

---
# Entreprise n°3 ~ *2018-2021*
![bg fit right:25%](assets/axians.jpg)

- **Type de contributions tech** :
  - Code & Project *Sponsor* : Suite Eyes 👀
  - Code contribution : Plugins de monitoring 

* **Type de contributions moins tech** :
  - Animation Meetups
  - Participation conférences
  - Aide à l'adoption en clientèle <!-- évangélisaton --> 

---
# Entreprise n°3 ~ *2018-2021*
![bg fit right:25%](assets/axians.jpg)

- **Faits marquants** :
  - Partenariat Editeurs Open Source
  - *End Of Life but not dead!*
    <!-- https://www.claudiokuenzler.com/blog/1044/check_infoblox-new-check-types-added -->

* **Bénéfices** :
  - Pour l'employeur : 💰💰, Confiance client, Visibilité
  - Pour l'employé : Reconnaissance, Crédibilité clients

---
# Entreprise n°4 ~ *2021-20??*
![bg fit right:25%](assets/LinkBynet-Part-of-Accenture.jpg)

- **Contexte** : ESN, DevOps & Cloud ☁️

- **Env. Tech** : Consulting DevOps & Cloud

- **Maturité** : Off course❗

- **Limites de contribution** :
  - C'est marqué dans le contrat‼️

  ```
  [...]
  Le salarié exercera la fonction de Consultant Cloud
  [...]
  Contribuer aux logiciels open source utilisés en interne ou pour les clients.
  [...]
  ```

---
<!--
_class: lead
paginate: false
footer: ''
-->

![bg fit left:25%](assets/logo_osxp_aplat-261x250.png)
# Comment contribuer ?<br />Le récap'

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Quoi, Comment
![bg fill right:38%](assets/sumup.gif)

- 📚 Documentation<!-- Oh mince, la doc ! -->, Retour de bugs

* Packaging 😈 <!-- Debian Orphaned Packages -->

* Projets annexes au métier
  - Forge logicielle,  Outillage
  - Supervision

* 🎤 Conférences, Meetups

* Support financier, Sponsoring

* Démissionner et aller bosser chez un éditeur Open Source 🫂

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Risques
![bg fill right:38%](assets/oops.gif)

- Bien en parler avec le Management, Direction
  - A qui appartient le code ?
  - Limite Pro / Perso (temps, investissement)
  - Managers & VP, libérez-nous !

* Licences des projets
  - "Contributor Programs"
  - Contamination

---
# Faire du business
![bg fill right:38%](assets/6m-rain.gif)

- Pistes
  - Choix des licences <!-- BSD -->
  - Gouvernance des projets choisis
    - Solutions sous fondation
    - Solutions avec souscription :
    deal avec éditeur

* Contribution & Sponsor != Edition

* Editeur de soft: **tout un métier !**
<!-- 09 Novembe 2021
14:20 - 14:40: Vous souhaitez passer votre projet en open source ? -->
---
<!--
_class: lead
paginate: false
footer: ''
-->

![bg fit left:25%](assets/logo_osxp_aplat-261x250.png)
# Comment contribuer ?<br />On y va !

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Conseils pour débuter

- Choix du projet
  - Projet que l'on utilise au quotidien
  - Technologie que l'on maîtrise
  - Langage connu
  - Vérifier l'*onboarding*

* Osez ! Lancez-vous, tant pis si ça foire

* Pas possible ? Pas grave !

![bg fit right:33%](assets/lespopcorn-Les-Bronzes-font-du-ski-Le-Splendide-Patrice-Leconte-Jean-Claude-Duss-fonce.gif)


---
# Mes contributions

![height:125px](assets/kubernetes-icon-color.png) ![height:120px](assets/prometheus-icon-color.png) ![height:125px](assets/Grafana_logo.svg)

![height:125px](assets/Ansible_logo.png) ![height:120px](assets/observatorium-logo.svg)

![height:100px](assets/eyes-logo.png)

![bg fit right:45%](assets/commits-2021.png)

---
# Et pourtant
<!-- commit Kubernetes -->
![bg 65%](assets/pr-kubernetes.png)

---
<!--
_class: lead
paginate: false
footer: ''
-->

![bg fit left:25%](assets/logo_osxp_aplat-261x250.png)
# Un dernier pour la route...

---
<!--
paginate: true
footer: Open Source Experience - 10/11/2021
-->

# Allons plus loin !

## OSPO & TODO Group

![width:500px](assets/ospo-2.png) ![width:600px](assets/todo-group.png)
<!--  09 novembre 2021
15:00 - 15:45 : Les OSPO comme catalyseurs d’une bonne gouvernance open source -->
---
# References

- [Ataxya@Twitter](https://twitter.com/AtaxyaNetwork/status/1433052248540594182?s=20)
- [Stéphane Philippart@L'Open Source au secours du développeur (et de l'architecte) ?](https://philippart-s.github.io/blog/articles/dev/oss-for-developer/)
- [Christophe Chaudier@La trouvaille du vendredi](https://www.youtube.com/watch?v=EgnljN5jaa4)

- [OpenSource Guide - 1](https://open-source-guide.com/Actualites/Comment-contribuer-a-un-projet-open-source-sans-etre-developpeur)
- [OpenSource Guide - 2](https://opensource.guide/fr/how-to-contribute/)
- [OSS For Developer](https://philippart-s.github.io/blog/articles/dev/oss-for-developer/)
- [Enterprise Open Source: A Practical Introduction](https://www.linuxfoundation.org/tools/enterprise-open-source-a-practical-introduction/)

---
<!--
_class: lead
paginate: false
footer: ''
-->

# Merci !
![width:970px](assets/DEVOPS-DDAY-Bandeau.jpg)

 #### OPENXP50 (50% pour les 10 premiers)

#### OPENXP30 (30% pour les autres)

### *Q&A : Stand B16, on embauche !*
